﻿public enum TileType
{
    INVALID,
    GRASS,
    WOOD,
    WATER,
    TREE,
    MUD,
    MUD_WHEAT_SEED,
    WHEAT
}

public enum Resource
{
    WOOD,
    NOTHING,
    BREAD,
    WHEAT
}

public enum BuildingAttributes
{
    WORKSHOP,
    STORAGE,
    HOME,
    CRAFTING
}

public enum ColorGraph
{
    WATER,
    EARTH,
    GRASS,
    NONE
}