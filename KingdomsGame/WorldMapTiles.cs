﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    public class WorldMapTiles
    {
        private Grid<WorldMapTile> tileGrid;
        private int numberOfTilesInMap = 0;

        public int NumberOfTilesInMap { get { return numberOfTilesInMap; } }
        public int Width { get { return tileGrid.Width; } }
        public int Height { get { return tileGrid.Height; } }

        public WorldMapTiles(int width, int height)
        {
            tileGrid = new Grid<WorldMapTile>(width, height);
        }

        /// <summary>
        /// Add the given WorldTile object to the world map at the given location
        /// </summary>
        /// <param name="worldTile">WorldTile object to add to world map</param>
        /// <param name="x">x co-ordinate</param>
        /// <param name="y">y co-ordinate</param>
        public void addWorldTileAt(WorldMapTile worldTile, int x, int y)
        {
            if (tileGrid.addItemAt(worldTile, x, y))
                worldTile.setLocation(x, y);
        }

        /// <summary>
        /// Get the world tile at the given x and y co-ordinates
        /// </summary>
        /// <param name="x">x co-ordinate</param>
        /// <param name="y">y co-ordinate</param>
        /// <returns>World map tile at location. Returns null if no tile is found</returns>
        public WorldMapTile getWorldTileAt(int x, int y)
        {
            return tileGrid.getItemAt(x, y);
        }

        /// <summary>
        /// Returns all tiles adjacent to the supplied tile. Supplied tile must be valid
        /// </summary>
        /// <param name="x">x-cordinate</param>
        /// <param name="y">y-cordinate</param>
        /// <returns>List of adjacent tiles</returns>
        public List<WorldMapTile> getSquareAdjacentTiles(int x, int y)
        {
            List<WorldMapTile> lstAdjTiles = new List<WorldMapTile>();

            if (getWorldTileAt(x, y) == null)
                return lstAdjTiles;


            if (x >= 0 && x <= 99 && y >= 0 && y <= 99)
            {
                lstAdjTiles.Add(getWorldTileAt(x - 1, y));
                lstAdjTiles.Add(getWorldTileAt(x + 1, y));
                lstAdjTiles.Add(getWorldTileAt(x, y - 1));
                lstAdjTiles.Add(getWorldTileAt(x, y + 1));
                lstAdjTiles.Add(getWorldTileAt(x - 1, y - 1));
                lstAdjTiles.Add(getWorldTileAt(x + 1, y - 1));
                lstAdjTiles.Add(getWorldTileAt(x - 1, y + 1));
                lstAdjTiles.Add(getWorldTileAt(x + 1, y + 1));
            }

            //Remove null entries if they have cropped up
            lstAdjTiles.RemoveAll(e => e == null);

            return lstAdjTiles;
        }

        /// <summary>
        /// Returns tiles in a cross adjacency to the supplied tile. Supplied tile must be valid
        /// </summary>
        /// <param name="x">x-cordinate</param>
        /// <param name="y">y-cordinate</param>
        /// <returns>List of adjacent tiles</returns>
        public List<WorldMapTile> getCrossAdjacentTiles(int x, int y)
        {
            List<WorldMapTile> lstAdjTiles = new List<WorldMapTile>();

            if (getWorldTileAt(x, y) == null)
                return lstAdjTiles;


            if (x >= 0 && x <= 99 && y >= 0 && y <= 99)
            {
                lstAdjTiles.Add(getWorldTileAt(x - 1, y));
                lstAdjTiles.Add(getWorldTileAt(x + 1, y));
                lstAdjTiles.Add(getWorldTileAt(x, y - 1));
                lstAdjTiles.Add(getWorldTileAt(x, y + 1));
            }

            //Remove null entries if they have cropped up
            lstAdjTiles.RemoveAll(e => e == null);

            return lstAdjTiles;
        }
    }
}
