﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Loaders;
using KingdomsGame.ScriptFacades;

namespace KingdomsGame
{
    public class ScriptRunner
    {
        private Script script;
        public GameController gameController;

        public ScriptRunner(GameController gameController)
        {
            this.gameController = gameController;
            initializeScripts();
        }

        public void initializeScripts()
        {
            UserData.RegisterAssembly();
            UserData.RegisterType<CharacterAction>();
            UserData.RegisterType<BuildAction>();
            UserData.RegisterType<WorldMapCharacter>();
            UserData.RegisterType<WorldMapHuman>();
            UserData.RegisterType<WorldMapBuilding>();
            UserData.RegisterType<PlayerCivilization>();
            UserData.RegisterType<WorldMapTile>();
            UserData.RegisterType(typeof(Resource));
            UserData.RegisterType(typeof(TileType));
            UserData.RegisterType(typeof(BuildingAttributes));
            UserData.RegisterType<KingdomsGame.WorldMapBuildingAttributes.StorageAttribute>();
            UserData.RegisterType<EntityMap<object,object>>();
            script = new Script();
            registerGlobals(script);
        }

        public void executeScript(string code)
        {
            try
            {
                script.DoString(code);
            }
            catch (ScriptRuntimeException e)
            {
                Console.WriteLine(e.DecoratedMessage);
            }
        }

        public void loadAndExecuteScript(string name)
        {
            loadAndExecuteScript(name, new Dictionary<string, string>());
        }

        public void loadAndExecuteScript(string name, string argName, string argData)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            d.Add(argName, argData);
            loadAndExecuteScript(name, d);
        }

        public void loadAndExecuteScript(string name, Dictionary<string, string> args)
        {
            Script s = new Script();

            s.Options.ScriptLoader = new EmbeddedResourcesScriptLoader();

            foreach (KeyValuePair<string, string> arg in args)
                s.Globals[arg.Key] = arg.Value;
            
            registerGlobals(s);

            try
            {
                s.DoFile("Content/Scripts/" + name + ".lua");
            }
            catch (ScriptRuntimeException e)
            {
                Console.WriteLine(e.DecoratedMessage);
            }
        }

        private void registerGlobals(Script s)
        {
            s.Globals["addChar"] = (Func<string, int, int, int>)
                gameController.addCharacterToGame;
            s.Globals["moveCharTo"] = (Action<int, int, int>)gameController.moveCharacterTo;
            s.Globals["setTimerInterval"] = (Action<float>)gameController.setTimerInterval;
            s.Globals["convertTile"] = (Action<int, int, string>)gameController.convertTile;
            s.Globals["workTile"] = (Action<int>)gameController.makeCharacterWorkTile;
            s.Globals["destroyTile"] = (Action<int, int, int>)gameController.makeCharacterDestroyTile;
            s.Globals["getChar"] = (Func<int, WorldMapCharacter>)gameController.WorldMap.getWorldMapCharacterById;
            s.Globals["getHuman"] = (Func<int, WorldMapHuman>)getWorldMapHuman;
            s.Globals["getBuilding"] = (Func<int, WorldMapBuilding>)gameController.WorldMap.getBuildingById;
            s.Globals["exec"] = (Action<string>)loadAndExecuteScript;
            s.Globals["getAllChars"] = (Func<IEnumerable<WorldMapCharacter>>)getAllScriptCharacters;
            s.Globals["build"] = (Func<string, int, int, int>)gameController.addBuildingToGame;
            s.Globals["checkBuildingIntegrity"] = (Func<int, bool>)gameController.checkBuildingIntegrity;
            s.Globals["putCharInBuilding"] = (Action<int, int>)gameController.WorldMap.putCharacterInBuilding;
            s.Globals["takeCharOutOfBuilding"] = (Action<int, int>)gameController.WorldMap.takeCharacterOutOfBuilding;
            s.Globals["buildOnTile"] = (Action<int>)gameController.makeCharacterBuildOnTile;
            s.Globals["Civ"] = gameController.PlayerCivilization;
            s.Globals["Resource"] = UserData.CreateStatic<Resource>();
            s.Globals["TileType"] = UserData.CreateStatic<TileType>();
            s.Globals["BuildingAttributes"] = UserData.CreateStatic<BuildingAttributes>();
        }

        private WorldMapHuman getWorldMapHuman(int id)
        {
            return (WorldMapHuman)gameController.WorldMap.getWorldMapCharacterById(id);
        }

        /// <summary>
        /// Creates a safe handle for the scripts to alter character data
        /// </summary>
        /// <returns>ScriptCharacter instance</returns>
        private WorldMapCharacterFacade getScriptCharacter(int charId)
        {
            return new WorldMapCharacterFacade(gameController.WorldMap.getWorldMapCharacterById(charId));
        }

        private WorldMapBuildingFacade getScriptBuilding(int id)
        {
            return new WorldMapBuildingFacade(gameController.WorldMap.getBuildingById(id));
        }

        private IEnumerable<WorldMapCharacter> getAllScriptCharacters()
        {
            foreach (WorldMapCharacter wmc in gameController.WorldMap.getAllWorldMapCharacters())
                yield return wmc;
        }
    }
}
