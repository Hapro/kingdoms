﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    public class WorldMapCharacters
    {
        private EntityMap<int, WorldMapCharacter> characterMap;

        public EntityMap<int, WorldMapCharacter> CharacterMap { get { return characterMap; } }

        public WorldMapCharacters()
        {
            characterMap = new EntityMap<int, WorldMapCharacter>();
        }

        public void removeCharacter(int id)
        {
            characterMap.remove(id);
        }

        /// <summary>
        /// Get a character on the world map by their id
        /// </summary>
        /// <param name="id">Id of character to find</param>
        /// <returns>The character, or null if none exists</returns>
        public WorldMapCharacter getCharacter(int id)
        {
            return characterMap.get(id);
        }

        /// <summary>
        /// Add a character to the game world. Assigns the character a unique id
        /// to be found in the game
        /// </summary>
        /// <param name="character">character to add to the game world</param>
        /// /// <returns>Id assigned to character so that it can be found</returns>
        public int addCharacter(WorldMapCharacter character)
        {
            int id = characterMap.Size + 1;
            characterMap.insert(id, character);
            character.Id = id;
            return id;
        }

        /// <summary>
        /// Returns true if a character is standing on this tile
        /// </summary>
        /// <param name="wmt">World map tile to check</param>
        /// <returns>True if a character is standing on the tile</returns>
        public bool doesTileHaveCharacter(WorldMapTile wmt)
        {
            foreach (KeyValuePair<int, WorldMapCharacter> wmc in characterMap.GetEnumerator)
            {
                if (wmc.Value.ParentTile == wmt)
                    return true;
            }

            return false;
        }
    }
}
