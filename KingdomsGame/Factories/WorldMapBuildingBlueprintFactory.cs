﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using KingdomsGame.Factories;

namespace KingdomsGame.Factories
{
    public class WorldMapBuildingBlueprintFactory : GameObjectFactory
    {
        protected override void init()
        {
          
        }

        protected override void readXML()
        {
            TileFactory wmtf = new TileFactory();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"Content\Data\blueprints.xml");
            XmlNodeList blueprintNodes = xmlDoc.GetElementsByTagName("blueprint");

            foreach (XmlNode n in blueprintNodes)
            {
                //<blueprint width=15 height=20 name="HUT">...</blueprint>
                WorldMapTiles worldMapTiles = new WorldMapTiles(
                    int.Parse(n.Attributes[0].Value),
                    int.Parse(n.Attributes[1].Value));

                foreach (XmlNode node in n.ChildNodes)
                {
                    //<tile x=0 y=3>STRAW_ROOF</tile>
                    int x = int.Parse(node.Attributes[0].Value);
                    int y = int.Parse(node.Attributes[1].Value);
                    TileType tType = (TileType)Enum.Parse(typeof(TileType), node.LastChild.Value);
                    worldMapTiles.addWorldTileAt(
                        (WorldMapTile)wmtf.create(tType), x, y);
                }

                dictObj.Add(n.Attributes[2].Value, new WorldMapBuildingBlueprint(worldMapTiles,
                    bool.Parse(n.Attributes[3].Value)));
            }

            base.readXML();
        }
    }
}
