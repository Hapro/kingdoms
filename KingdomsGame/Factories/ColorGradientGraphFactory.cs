﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KingdomsGame.Factories;
using Microsoft.Xna.Framework;

namespace KingdomsGame.Factories
{
    public class ColorGradientGraphFactory : GameObjectFactory
    {
        protected override void init()
        {
            ColorGradientGraph water = new ColorGradientGraph();
            ColorGradientGraph earth = new ColorGradientGraph();
            ColorGradientGraph grass = new ColorGradientGraph();
            ColorGradientGraph none = new ColorGradientGraph();

            water.ColorSamples = new List<KeyValuePair<double, Color>>();
            water.ColorSamples.Add(new KeyValuePair<double, Color>(0.0, Color.DarkBlue));
            water.ColorSamples.Add(new KeyValuePair<double, Color>(0.6, Color.Cyan));
            water.ColorSamples.Add(new KeyValuePair<double, Color>(1.0, Color.Cyan));

            earth.ColorSamples = new List<KeyValuePair<double, Color>>();
            earth.ColorSamples.Add(new KeyValuePair<double, Color>(0.0, Color.BurlyWood));
            earth.ColorSamples.Add(new KeyValuePair<double, Color>(0.6, Color.Brown));
            earth.ColorSamples.Add(new KeyValuePair<double, Color>(1.0, Color.RosyBrown));

            grass.ColorSamples = new List<KeyValuePair<double, Color>>();
            grass.ColorSamples.Add(new KeyValuePair<double, Color>(0.0, Color.DarkGreen));
            grass.ColorSamples.Add(new KeyValuePair<double, Color>(0.40, Color.SandyBrown));
            grass.ColorSamples.Add(new KeyValuePair<double, Color>(0.50, Color.Green));
            grass.ColorSamples.Add(new KeyValuePair<double, Color>(1.0, Color.LightGreen));

            none.ColorSamples = new List<KeyValuePair<double, Color>>();
            none.ColorSamples.Add(new KeyValuePair<double, Color>(0.0, Color.White));
            none.ColorSamples.Add(new KeyValuePair<double, Color>(1.0, Color.White));

            dictObj.Add(ColorGraph.EARTH, earth);
            dictObj.Add(ColorGraph.WATER, water);
            dictObj.Add(ColorGraph.GRASS, grass);
            dictObj.Add(ColorGraph.NONE, none);
        }
    }
}
