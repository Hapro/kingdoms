﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Xml;

namespace KingdomsGame.Factories
{
    /// <summary>
    /// Provides methods for creating tiles
    /// </summary>
    public class TileFactory : GameObjectFactory
    {
        EntityMap<TileType, TileChangeTimer> tileChangeTimerMap;

        protected override void init()
        {
            tileChangeTimerMap = new EntityMap<TileType, TileChangeTimer>();
        }

        protected override void readXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"Content\Data\tiles.xml");
            XmlNodeList buildingNodes = xmlDoc.GetElementsByTagName("tile");

            foreach (XmlNode n in buildingNodes)
            {
                //<tile tileType="WOOD">...</building>
                string buildingName = n.Attributes[0].Value;
                TileType tileType = (TileType)Enum.Parse(typeof(TileType), buildingName);
                bool passable = bool.Parse(n.ChildNodes[0].InnerText);
                ColorGraph colorGraph = (ColorGraph)Enum.Parse(typeof(ColorGraph),
                    n.ChildNodes[1].InnerText);

                dictObj.Add(tileType, new WorldMapTile(tileType,
                    passable,
                    Color.White, colorGraph));
            }
            readTileChangeTimerFile();
        }

        private void readTileChangeTimerFile()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"Content\Data\tileChangeTimers.xml");
            XmlNodeList buildingNodes = xmlDoc.GetElementsByTagName("tileChangeTimer");

            foreach (XmlNode n in buildingNodes)
            {
                string tileName = n.Attributes[0].Value;
                TileType tileType = (TileType)Enum.Parse(typeof(TileType), tileName);
                TileType tileToChangeTo = (TileType)Enum.Parse(typeof(TileType),
                    n.ChildNodes[0].InnerText);
                int turns = int.Parse(n.ChildNodes[1].InnerText);

                tileChangeTimerMap.insert(tileType,
                    new TileChangeTimer(turns, tileToChangeTo));
            }
        }

        public object create(object type, out TileChangeTimer tileChangeTimer)
        {
            WorldMapTile wmt = (WorldMapTile)base.create(type);
            tileChangeTimer = tileChangeTimerMap.get(wmt.TileType);
            if(tileChangeTimer != null)
                tileChangeTimer.WorldMapTile = wmt;
            return wmt;
        }
    }
}
