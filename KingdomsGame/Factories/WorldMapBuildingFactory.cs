﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using KingdomsGame.WorldMapBuildingAttributes;

namespace KingdomsGame.Factories
{
    public class WorldMapBuildingFactory : GameObjectFactory
    {
        WorldMapBuildingBlueprintFactory wmbbf;

        protected override void init()
        {
            wmbbf = new WorldMapBuildingBlueprintFactory();
        }

        private CraftingAttribute loadCraftingBuilding(XmlNode node)
        {
            //TOOD: only load certain items based on what type of building this is
            ResourceCraftingMap rcm = new ResourceCraftingMap();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"Content\Data\crafting.xml");
            XmlNodeList craftingNodes = xmlDoc.GetElementsByTagName("crafting");

            foreach (XmlNode n in craftingNodes)
            {
                  //<crafting resource="BREAD">
                    //<req amount="2">WHEAT</req>

                Resource resToMake = (Resource)Enum.Parse(typeof(Resource), n.Attributes[0].Value);
                List<KeyValuePair<Resource, int>> lst = new List<KeyValuePair<Resource,int>>();
                string name = n.Attributes[1].Value;

                foreach(XmlNode n2 in n.ChildNodes)
                {
                    Resource resRequired = (Resource)Enum.Parse(typeof(Resource), n2.InnerText);
                    int amount = int.Parse(n2.Attributes[0].Value);
                    lst.Add(new KeyValuePair<Resource,int>(resRequired, amount));
                }
                rcm.addCraftingItem(resToMake, new CraftingItem(resToMake, lst, name));

            }
            return new CraftingAttribute(rcm);

        }

        protected override void readXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"Content\Data\buildings.xml");
            XmlNodeList buildingNodes = xmlDoc.GetElementsByTagName("building");

            foreach (XmlNode n in buildingNodes)
            {
                //<building name="Straw storage hut">...</building>
                string buildingName = n.Attributes[0].Value;
                WorldMapBuilding wmb = new WorldMapBuilding((WorldMapBuildingBlueprint)
                    wmbbf.create(buildingName), buildingName);

                foreach (XmlNode node in n.ChildNodes)
                {
                    //<attribute>STORAGE</attribute>
                    switch((BuildingAttributes)Enum.Parse(typeof(BuildingAttributes),
                        node.LastChild.Value))
                    {
                        case BuildingAttributes.STORAGE:
                            wmb.BuildingAttributesList.insert(BuildingAttributes.STORAGE, 
                                new StorageAttribute());
                            break;

                        case BuildingAttributes.WORKSHOP:
                            wmb.BuildingAttributesList.insert(BuildingAttributes.WORKSHOP,
                                new WorkshopAttribute());
                                break;

                        case BuildingAttributes.HOME:
                                wmb.BuildingAttributesList.insert(BuildingAttributes.HOME,
                                    new HomeAttribute());
                                break;

                        case BuildingAttributes.CRAFTING:
                                wmb.BuildingAttributesList.insert(BuildingAttributes.CRAFTING,
                                    loadCraftingBuilding(n));
                                break;
                    }
                    
                }
                dictObj.Add(buildingName, wmb);
            }

            base.readXML();
        }
    }
}
