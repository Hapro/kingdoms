﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame.Factories
{
    public abstract class GameObjectFactory
    {
        //Contains intantiated objects to be copied into the game world
        protected Dictionary<object, ICloneable> dictObj;

        public Dictionary<object, ICloneable> Dictionary
        {
            get { return dictObj; }
        }

        public GameObjectFactory()
        {
            this.dictObj = new Dictionary<object, ICloneable>();
            init();
            readXML();
        }

        protected abstract void init();

        public virtual object create(object type)
        {
            if (dictObj.ContainsKey(type))
                return dictObj[type].Clone();

            return null;
        }

        protected virtual void readXML()
        {
        }
    }
}
