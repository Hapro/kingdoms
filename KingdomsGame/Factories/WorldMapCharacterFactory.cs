﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame.Factories
{
    public class WorldMapCharacterFactory : GameObjectFactory
    {
        protected override void init()
        {
            dictObj.Add("HUMAN",
                new WorldMapHuman());
        }
    }
}
