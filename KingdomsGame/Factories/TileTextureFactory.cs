﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace KingdomsGame.Factories
{
    class TileTextureFactory : GameObjectFactory
    {
        protected override void init()
        {
        }

        protected override void readXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"Content\Data\tileTextures.xml");
            XmlNodeList buildingNodes = xmlDoc.GetElementsByTagName("tileTextures");

            foreach (XmlNode n in buildingNodes[0].ChildNodes)
            {
                string tileName = n.Attributes[0].Value;
                TileType tileType = (TileType)Enum.Parse(typeof(TileType), tileName);
                string textureName = n.ChildNodes[0].InnerText;

                dictObj.Add(tileType, textureName);
            }
        }
    }
}
