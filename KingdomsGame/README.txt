﻿SUMMARY
KINGDOMS is a game where you lead a group of hunter-gatherers to form a city-state and beyond.

GAMEPLAY
Players do not control each denizen individually. Rather, they give broad instructions
which are automatically carried out by suitable members of the population.

For example to clear a forest you need to select a workshop building. Then you can 
issue 'work tile' commands on a tree tile. When a tree tile is 'worked', it is replaced
by grass, and the worker takes the wood to the nearest store room.

The world map is generated using a perlin noise algorithm.
 

