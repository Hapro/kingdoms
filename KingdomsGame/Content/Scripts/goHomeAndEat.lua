﻿--When a character is hungry, they will automatically go home,
--eat, then return to their original post
--params:
--	charID : ID of character this script is controlling

local i = getHuman(tonumber(charID));
local state = i.State;

if state == "ready" then						--move home

	moveCharTo(i.Id, 
		i.HomeBuilding.EntranceX,
		i.HomeBuilding.EntranceY
		);
	i.state = "GR_0"

elseif state == "GR_0" then

	if i.AtEndOfPath then						--if at home	
		putCharInBuilding(i.HomeBuilding.ID,	--Enter home
			i.Id);
		i.state = "GR_1"
	end

elseif state == "GR_1" then
	--todo: eat
	i.Hunger = 100
	i.state = "GR_2"
elseif state == "GR_2" then
	moveCharTo(i.Id, 
		i.WorkBuilding.EntranceX,
		i.WorkBuilding.EntranceY
		);
	i.state = "GR_3"

elseif state == "GR_3" then
	if i.AtEndOfPath then						--if at work	
		putCharInBuilding(i.WorkBuilding.ID,	--Enter work
			i.Id);
		i.idle();
	end
end