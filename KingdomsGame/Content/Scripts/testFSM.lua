﻿local c = getChar(tonumber(charID));

if c.hasPath() == false then
	if(c.X() == 0 and c.Y() == 0) then
		moveCharTo(c.ID(), 5, 0);
	end

	if(c.X() == 5 and c.Y() == 0) then
		moveCharTo(c.ID(), 5, 5);
	end

	if(c.X() == 5 and c.Y() == 5) then
		moveCharTo(c.ID(), 0, 5);
	end

	if(c.X() == 0 and c.Y() == 5) then
		moveCharTo(c.ID(), 0, 0);
	end
end