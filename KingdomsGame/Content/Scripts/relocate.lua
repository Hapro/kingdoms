﻿local i = getChar(tonumber(charID));
local state = i.State;

if state == "ready" then
	moveCharTo(i.Id, 
		i.WorkBuilding.EntranceX,
		i.WorkBuilding.EntranceY
		);

	i.State = "GR_0";
elseif state=="GR_0" then
	if i.AtEndOfPath then
		local bID = i.WorkBuilding.ID;
		putCharInBuilding(bID,	--Enter building
			i.Id);
		i.idle();								--Return to idle state
	end
end