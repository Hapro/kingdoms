﻿--Script to tell character how to build stuff
--params:
--	charID : ID of character this script is controlling

local i = getChar(tonumber(charID));
local state = i.State;

if state == "ready" then						--begin move to storage building
	local wID = i.Data["CharacterAction"].WorldMapBuilding.ID;
	local storageBuilding;

	for x in Civ.getRelatedBuildings(wID, BuildingAttributes.STORAGE) do
		i.Data["StorageID"] = x;
		storageBuilding = getBuilding(x);
	end

	moveCharTo(i.Id, 
		storageBuilding.EntranceX,
		storageBuilding.EntranceY);

	i.State = "GR_0";

elseif state == "GR_0" then
	if i.AtEndOfPath then						--if at storage
		putCharInBuilding(i.data["StorageID"],	--Enter storage building
			i.Id);								
		i.State = "GR_1";					
	end

elseif state == "GR_1" then						--check if storage contains item
	local sb = getBuilding(i.Data["StorageID"]);
	local store = sb.getStorageAttribute();
	local resourceNeeded = i.Data["CharacterAction"].ResourceToUse;

	if store.containsResource(resourceNeeded) then			--if it does, take
		i.HeldResource = store.removeResource(resourceNeeded);
		moveCharTo(i.Id,									--move to loc to build
			i.Data["CharacterAction"].WorldMapTile.X,
			i.Data["CharacterAction"].WorldMapTile.Y);
		i.State = "GR_2";
	else
		moveCharTo(i.Id,			--go back to workshop
			i.Data["CharacterAction"].WorldMapBuilding.EntranceX,
			i.Data["CharacterAction"].WorldMapBuilding.EntranceY);
		i.State = "GR_3"
	end

elseif state == "GR_2" then
	if i.AtEndOfPath then
		buildOnTile(i.Id);			-- Build
		moveCharTo(i.Id,			--go back to workshop
			i.Data["CharacterAction"].WorldMapBuilding.EntranceX,
			i.Data["CharacterAction"].WorldMapBuilding.EntranceY);
		i.State = "GR_3"
	end

elseif state == "GR_3" then
	if i.AtEndOfPath then						--if at workshop	
		local bID = i.Data["CharacterAction"].WorldMapBuilding.ID;
		putCharInBuilding(bID,	--Enter workshop
			i.Id);
		i.Data["CharacterAction"].Finished = true
		i.idle();								--Return to idle state
	end
end