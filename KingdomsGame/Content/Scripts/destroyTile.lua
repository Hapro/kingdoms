﻿--Script to tell character how to destroy tiles
--params:
--	charID : ID of character this script is controlling

local i = getChar(tonumber(charID));
local state = i.State;

if state == "ready" then						--begin move to tile
	local bID = i.Data["CharacterAction"].WorldMapBuilding.ID;

	moveCharTo(i.Id, 
		i.Data["AdjacentTile"].X, 
		i.Data["AdjacentTile"].Y
		);
		--Destroy table will find nearest adjacent tile that is clear

	i.State = "GR_0";

elseif state == "GR_0" then
	if i.AtEndOfPath then						--if adjacent to the tile to destroy
		local tile = i.Data["CharacterAction"].WorldMapTile
		destroyTile(i.Id, tile.X, tile.Y);		--destroy tile
		i.State = "GR_1";					
	end

elseif state == "GR_1" then
	local b = i.Data["CharacterAction"].WorldMapBuilding; --return to worksop
	moveCharTo(i.Id, b.EntranceX, b.EntranceY);
	i.State = "GR_4";

elseif state =="GR_4" then
	if i.AtEndOfPath then						--if at workshop	
		local bID = i.Data["CharacterAction"].WorldMapBuilding.ID;
		putCharInBuilding(bID,	--Enter workshop
			i.Id);
		i.Data["CharacterAction"].Finished = true
		i.idle();								--Return to idle state
	end
end