﻿--Script to tell character how to gather resources
--params:
--	charID : ID of character this script is controlling

local i = getChar(tonumber(charID));
local state = i.State;

if state == "ready" then						--begin move to tile
	local bID = i.Data["CharacterAction"].worldMapBuilding.ID;

	for x in Civ.getRelatedBuildings(bID, BuildingAttributes.STORAGE) do
		i.Data["StorageID"] = x;
	end

	moveCharTo(i.Id, 
		i.Data["CharacterAction"].WorldMapTile.X, 
		i.Data["CharacterAction"].WorldMapTile.Y
		);

	i.State = "GR_0";

elseif state == "GR_0" then
	if i.AtEndOfPath then						--if at tile to work	
		workTile(i.Id);							--Harvest resource
		i.State = "GR_1";					
	end

elseif state == "GR_1" then						--Move to storage 
	local b = getBuilding(i.data["StorageID"]);
	moveCharTo(i.Id, b.EntranceX, b.EntranceY);
	i.State = "GR_2";

elseif state =="GR_2" then
	if i.AtEndOfPath then						--if at storage	
		putCharInBuilding(i.data["StorageID"],	--Enter storage building
			i.Id);
		getBuilding(i.data["StorageID"]).getStorageAttribute()
			.addResource(i.takeResource());
		i.State = "GR_3";
	end

elseif state == "GR_3" then						--if items put inside storage
	local b = i.Data["CharacterAction"].WorldMapBuilding; --return to worksop
	moveCharTo(i.Id, b.EntranceX, b.EntranceY);
	i.State = "GR_4";

elseif state =="GR_4" then
	if i.AtEndOfPath then						--if at workshop	
		local bID = i.Data["CharacterAction"].WorldMapBuilding.ID;
		putCharInBuilding(bID,	--Enter workshop
			i.Id);
		i.Data["CharacterAction"].Finished = true
		i.idle();								--Return to idle state
	end
end