﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using MonoGame.Framework;

namespace KingdomsGame
{
    class PathFindingStep
    {
        public int g; //cost-so-far
        public int h; //manhattan distance to goal
        public int x;
        public int y;
        public PathFindingStep parent;

        public PathFindingStep(int x, int y, int g, int h)
        {
            this.x = x;
            this.y = y;
            this.g = g;
            this.h = h;
        }
    }
    public class PathFinder
    {
        //TODO: Implement optimised data structures 
        WorldMapTiles worldMapTiles;

        public PathFinder(WorldMapTiles worldMapTiles)
        {
            this.worldMapTiles = worldMapTiles;
        }

        bool isPFSInList(PathFindingStep pfs, List<PathFindingStep> lstPfs)
        {
            foreach (PathFindingStep p in lstPfs)
            {
                if (p.x == pfs.x && p.y == pfs.y)
                    return true;
            }

            return false;
        }

        PathFindingStep findLowestCostStepInList(List<PathFindingStep> lstPfs)
        {
            PathFindingStep pf = lstPfs[0];

            foreach (PathFindingStep p in lstPfs)
            {
                if ((pf.g + pf.h) > (p.g + p.h))
                    pf = p;
            }

            return pf;
        }

        List<Vector2> reconstructPath(PathFindingStep pfs)
        {
            List<Vector2> lstPath = new List<Vector2>();
            PathFindingStep p = pfs;

            do
            {
                lstPath.Add(new Vector2(p.x, p.y));
                p = p.parent;
            } while (p != null);

            return lstPath;
        }

        public List<Vector2> findPath(int startX, int startY, int goalX, int goalY)
        {
            List<PathFindingStep> lstClosed = new List<PathFindingStep>();
            List<PathFindingStep> lstOpen = new List<PathFindingStep>();
            PathFindingStep currentTile;

            lstOpen.Add(new PathFindingStep(startX, startY, 0, 0));
            
            do
            {
                currentTile = findLowestCostStepInList(lstOpen);
                lstOpen.Remove(currentTile);
                lstClosed.Add(currentTile);

                if (currentTile.x == goalX && currentTile.y == goalY)
                {
                    return reconstructPath(currentTile);
                }

                WorldMapTile wmt = worldMapTiles.getWorldTileAt(currentTile.x, currentTile.y);

                foreach (WorldMapTile wt in
                    worldMapTiles.getSquareAdjacentTiles(currentTile.x, currentTile.y))
                {
                    PathFindingStep p = new PathFindingStep(wt.X, wt.Y, 0, 0);
                    p.parent = currentTile;

                    if (!isPFSInList(p, lstClosed) && wt.Passable)
                    {
                        int x = (wt.X - goalX);
                        int y = (wt.Y - goalY);
                        int h = x + y;

                        //diagonal movement more expensive
                        if (p.x != currentTile.x && p.y != currentTile.y)
                            p.g = currentTile.g + 2;
                        else
                            p.g = currentTile.g + 1;
                        
                        p.h = h;

                        if (!isPFSInList(p, lstOpen))                        
                        {
                            lstOpen.Add(p);
                        }
                        else
                        {
                            PathFindingStep tp = lstOpen.Find(e => e.x == p.x && e.y == p.y);
                            if((tp.g + tp.h) > (p.g + p.h))
                            {
                                tp = p;
                            }
                        }
                        
                    }
                }
            } while (lstOpen.Count > 0);

            return new List<Vector2>();
        }
    }
}
