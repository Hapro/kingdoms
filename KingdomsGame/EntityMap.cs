﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoonSharp.Interpreter;

namespace KingdomsGame
{
    /// <summary>
    /// Maps one entity to another
    /// i.e. map character IDs to buildings IDs
    /// or a building ID to a list of character IDs
    /// </summary>
    [MoonSharpUserData]
    public class EntityMap<T, U>
    {
        private Dictionary<T, U> dictMap;
        public int Size { get { return dictMap.Count; } }
        public IEnumerable<KeyValuePair<T, U>> GetEnumerator
        {
            get
            { 
                foreach(KeyValuePair<T, U> kvp in dictMap)
                    yield return kvp; 
            }
        }

        public EntityMap()
        {
            dictMap = new Dictionary<T, U>();
        }

        public U this[T i]
        {
            get { return get(i); }
            set { insert(i, value); }
        }

        public void clear()
        {
            dictMap.Clear();
        }

        public virtual void remove(T key)
        {
            if (exists(key))
                dictMap.Remove(key);
        }


        /// <summary>
        /// Adds a new key to the map. If the key already exists
        /// the key is updated
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public virtual void insert(T key, U value)
        {
            if(!exists(key))
                dictMap.Add(key, value);
            else
            {
                remove(key);
                insert(key, value);
            }
        }

        public virtual U get(T key)
        {
            if(exists(key))
                return dictMap[key];

            return default(U);
        }

        public virtual U take(T key)
        {
            U val = get(key);
            remove(key);
            return val;
        }

        public virtual bool exists(T key)
        {
            return dictMap.ContainsKey(key);
        }
    }
}
