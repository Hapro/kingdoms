﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace KingdomsGame
{
    /// <summary>
    /// Describes the location of the tiles required for 
    /// a building to be valid
    /// 
    /// i.e. a valid hut is 4 'straw roof' tiles in a square
    /// </summary>
    public class WorldMapBuildingBlueprint : ICloneable
    {
        private WorldMapTiles tileBlueprint;
        private Rectangle buildingBoundary;
        private bool permanent;

        public WorldMapTiles Tiles
        {
            get { return tileBlueprint; }
        }

        public Rectangle BuildingBoundary
        {
            get { return buildingBoundary; }
        }

        public bool Permanent
        {
            get { return permanent; }
        }

        public WorldMapBuildingBlueprint(WorldMapTiles wmt, bool trackByCivilization)
        {
            buildingBoundary = new Rectangle(0, 0, wmt.Width, wmt.Height);
            tileBlueprint = wmt;
            this.permanent = trackByCivilization;
        }

        public bool checkBuildingIntegrity(WorldMapBuilding building, WorldMap worldMap)
        {
            //Convert global loc of building's boundary to 
            //local co-ordinates
            int xOffset = (int)building.Location.X;
            int yOffset = (int)building.Location.Y;

            for (int x = 0; x < BuildingBoundary.Width; x++)
            {
                for (int y = 0; y < BuildingBoundary.Height; y++)
                {
                    if (worldMap.getWorldTileAt(x + xOffset, y + yOffset).TileType !=
                        tileBlueprint.getWorldTileAt(x, y).TileType)
                        return false;
                }
            }

            return true;
        }

        public virtual object Clone()
        {
            WorldMapTiles wmtb = new WorldMapTiles(this.tileBlueprint.Width,
                this.tileBlueprint.Height);
            WorldMapBuildingBlueprint wmbb = new WorldMapBuildingBlueprint(wmtb,
                this.permanent);

            for(int x = 0; x < tileBlueprint.Width; x++)
            {
                for(int y = 0; y <tileBlueprint.Height; y++)
                {
                    WorldMapTile wmt = (WorldMapTile)this.tileBlueprint.getWorldTileAt(x, y).Clone();
                    wmbb.tileBlueprint.addWorldTileAt(wmt, x, y);
                }
            }

            wmbb.buildingBoundary = new Rectangle(
                this.buildingBoundary.X,
                this.buildingBoundary.Y,
                this.buildingBoundary.Width,
                this.buildingBoundary.Height);

            return wmbb;
        }
    }
}
