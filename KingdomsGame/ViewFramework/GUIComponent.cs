﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    /// <summary>
    /// Base class for GUI components. A GUI component is 
    /// a part of the gui responsible for a certain
    /// thing. I.e. we may have pop-up boxes etc
    /// </summary>
    public abstract class GUIComponent
    {
        protected GameView gameView;
        protected GameController gameController;
        protected ViewportManager viewportManager;
        protected List<GUIControl> lstGUIControls;
        protected ContentManager content;

        protected const int GRID_SIZE = 50;

        public GUIComponent(GameView gameView, GameController gameController,
            ViewportManager viewportManager)
        {
            this.gameController = gameController;
            this.gameView = gameView;
            this.viewportManager = viewportManager;
            this.lstGUIControls = new List<GUIControl>();
        }

        public virtual void doViewportManagerResolutionChange(object sender, EventArgs e)
        {
            scaleGUIToViewport();
        }

        public Button addButton(Button b)
        {
            return (Button)addGUIControl(b);
        }

        public GUIControl addGUIControl(GUIControl c)
        {
            this.lstGUIControls.Add(c);
            return c;
        }

        /// <summary>
        /// Add gui control specifiying x and y grid cord
        /// </summary>
        /// <param name="c">control</param>
        /// <param name="x">grid x location</param>
        /// <param name="y">grid y location</param>
        /// <returns></returns>
        public GUIControl addGUIControl(GUIControl c, int x, int y)
        {
            this.lstGUIControls.Add(c);
            c.Location = new Vector2(x * GRID_SIZE, 
                y * GRID_SIZE);
            return c;
        }

        public void removeGUIControl(GUIControl c)
        {
            this.lstGUIControls.Remove(c);
        }

        public void update(UserInput userInput)
        {
            foreach (GUIControl c in lstGUIControls)
                c.update(userInput);
        }

        public void draw(SpriteBatch sb)
        {
            foreach (GUIControl c in lstGUIControls)
                c.draw(sb);
        }

        public abstract void scaleGUIToViewport();

        public void loadContent(ContentManager content)
        {
            this.content = content;

            foreach (GUIControl gc in lstGUIControls)
                gc.loadContent(content);
        }
    }
}
