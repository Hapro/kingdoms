﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace KingdomsGame.ViewFramework
{
    public class UserInput
    {
        private MouseState mouseState;
        private MouseState oldMouseState;
        private KeyboardState keyboardState;
        private KeyboardState oldKeyboardState;
        private Vector2 viewportScale; 

        /// <summary>
        /// Value used to scale mouse position to screen size
        /// </summary>
        public Vector2 ViewportScale
        {
            get { return viewportScale; }
            set { viewportScale = value; }
        }

        public bool LeftClick
        {
            get
            {
                return mouseState.LeftButton == ButtonState.Pressed
                && oldMouseState.LeftButton != ButtonState.Pressed;
            }
        }

        public bool RightClick
        {
            get
            {
                return mouseState.LeftButton == ButtonState.Pressed
                    && oldMouseState.LeftButton != ButtonState.Pressed;
            }
        }

        public Vector2 MouseLoc
        {
            get
            {
                return new Vector2(Mouse.GetState().X / viewportScale.X,
                    Mouse.GetState().Y / viewportScale.Y);
            }
        }

        public UserInput(Vector2 viewportScale)
        {
            this.viewportScale = viewportScale;
        }

        public void doViewportManagerResolutionChange(object sender, EventArgs e)
        {
            updateViewportScale((ViewportManager)sender);
        }

        public void updateViewportScale(ViewportManager viewportManager)
        {
            viewportScale = viewportManager.ViewportScale;
        }

        public bool keyPressed(Keys key)
        {
            return keyboardState.IsKeyDown(key) && !oldKeyboardState.IsKeyDown(key);
        }

        public void update()
        {
            oldMouseState = mouseState;
            oldKeyboardState = keyboardState;
            mouseState = Mouse.GetState();
            keyboardState = Keyboard.GetState();
        }
    }
}
