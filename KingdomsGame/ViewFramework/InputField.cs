﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace KingdomsGame.ViewFramework
{
    public class InputField : GUIControl
    {
        private string buffer;
        private Keys[] acceptableKeys;
        private bool focus;
        private Button confirm;

        public OnInputSubmit InputSubmit = delegate { };

        public string Data
        {
            get { return buffer; }
        }

        public InputField()
        {
            int i = 0;
            acceptableKeys = new Keys[36];
            acceptableKeys[i++] = Keys.Q;
            acceptableKeys[i++] = Keys.W;
            acceptableKeys[i++] = Keys.E;
            acceptableKeys[i++] = Keys.R;
            acceptableKeys[i++] = Keys.T;
            acceptableKeys[i++] = Keys.Y;
            acceptableKeys[i++] = Keys.U;
            acceptableKeys[i++] = Keys.I;
            acceptableKeys[i++] = Keys.O;
            acceptableKeys[i++] = Keys.P;
            acceptableKeys[i++] = Keys.A;
            acceptableKeys[i++] = Keys.S;
            acceptableKeys[i++] = Keys.D;
            acceptableKeys[i++] = Keys.F;
            acceptableKeys[i++] = Keys.G;
            acceptableKeys[i++] = Keys.H;
            acceptableKeys[i++] = Keys.J;
            acceptableKeys[i++] = Keys.K;
            acceptableKeys[i++] = Keys.L;
            acceptableKeys[i++] = Keys.Z;
            acceptableKeys[i++] = Keys.X;
            acceptableKeys[i++] = Keys.C;
            acceptableKeys[i++] = Keys.V;
            acceptableKeys[i++] = Keys.B;
            acceptableKeys[i++] = Keys.N;
            acceptableKeys[i++] = Keys.M;
            acceptableKeys[i++] = Keys.D0;
            acceptableKeys[i++] = Keys.D1;
            acceptableKeys[i++] = Keys.D2;
            acceptableKeys[i++] = Keys.D3;
            acceptableKeys[i++] = Keys.D4;
            acceptableKeys[i++] = Keys.D5;
            acceptableKeys[i++] = Keys.D6;
            acceptableKeys[i++] = Keys.D7;
            acceptableKeys[i++] = Keys.D8;
            acceptableKeys[i++] = Keys.D9;

            buffer = text = "";

            confirm = new Button("Confirm", new Vector2(0, 40));
            confirm.Clicked += new OnClickHandler(doConfirmClicked);
            addChildControl(confirm);
        }

        public void clear() { buffer = ""; }

        public override Rectangle Bounds
        {
            get
            {
                if (font.MeasureString(text).X > 100)
                    return base.Bounds;
                else
                    return new Rectangle(
                        (int)location.X, (int)location.Y, 100, 20);
            }
        }

        private string convertKeyToCharacter(Keys key)
        {
            if(key != Keys.D0 && key != Keys.D2 && key != Keys.D3 && key != Keys.D4 &&
                key != Keys.D5 && key != Keys.D6 && key != Keys.D7 &&key != Keys.D8 &&
                key != Keys.D9)
            {
                return key.ToString();
            }
            else
            {
                return key.ToString().Substring(1, 1);
            }
        }

        public override void update(UserInput userInput)
        {
            if (focus)
                color = Color.DimGray;
            else
                color = Color.Gray;

            if(Bounds.Contains(userInput.MouseLoc))
            {
                if (!focus)
                    focus = userInput.LeftClick;
            }
            else
            {
                if (userInput.LeftClick)
                    focus = false;
            }

            if (focus)
            {
                foreach (Keys k in acceptableKeys)
                {
                    if (userInput.keyPressed(k))
                    {
                        if (userInput.keyPressed(Keys.LeftShift) || userInput.keyPressed(Keys.RightShift))
                            buffer += convertKeyToCharacter(k);
                        else
                            buffer += convertKeyToCharacter(k).ToLower();
                    }
                }

                if (userInput.keyPressed(Keys.Back) && buffer.Count() > 0)
                    buffer = buffer.Remove(buffer.Count() - 1);

            }
            this.text = buffer;

            base.update(userInput);
        }

        public void doConfirmClicked(object sender, ButtonClickArgs e)
        {
            InputSubmit(this, new InputSubmitArgs(Data));
            clear();
        }
    }
}
