﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace KingdomsGame.ViewFramework
{
    class SidebarDestroyTile : Sidebar
    {
        private WorldMapBuilding selectedBuilding;
        private Button backButton;

        public SidebarDestroyTile(GameView gameView, GameController gameController,
            ViewportManager viewportManager, WorldMapBuilding selectedBuilding)
            : base(gameView, gameController, viewportManager)
        {
            backButton = addButton(new Button("Back", new Vector2(20, 20)));
            infoPanel.Text = "Select a tile to destroy.";
            backButton.Clicked += new OnClickHandler(doBackButtonClicked);
            this.selectedBuilding = selectedBuilding;
        }


        private void doBackButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarSelectedBuilding(gameView,
                gameController, viewportManager, selectedBuilding);
        }

        public override void doWorldMapOverlayTileSelect(object sender, TileSelectArgs e)
        {
            gameController.PlayerCivilization.addDestroyTileAction(
                selectedBuilding,
                e.worldMapTile);
        }
    }
}
