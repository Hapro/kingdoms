﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    class SidebarSelectedPlayerBuilding : SidebarSelectedBuilding
    {
        private Button workTileModeButton;
        private Button buildTileModeButton;
        private Button destroyTileModeButton;
        private Button craftResourceModeButton;
        private InputField addRelationInput;

        public SidebarSelectedPlayerBuilding(GameView gameView, GameController gameController,
            ViewportManager viewportManager, WorldMapBuilding selectedBuilding)
            : base(gameView, gameController, viewportManager, selectedBuilding)
        {
            infoPanel.Text = "Owned\n" + selectedBuilding.ToString();

            infoPanel.Text += "Related Buildings:\n";

            foreach(int i in gameController.PlayerCivilization.getRelatedBuildings(selectedBuilding.ID))
            {
                infoPanel.Text += i.ToString() + ",";
            }

            workTileModeButton = addButton(new Button("Work tile", new Vector2(0, 300)));
            buildTileModeButton = addButton(new Button("Build", new Vector2(0, 330)));
            destroyTileModeButton = addButton(new Button("Destroy tile", new Vector2(0, 360)));
            craftResourceModeButton = addButton(new Button("Craft resource", new Vector2(0, 390)));
            addRelationInput = (InputField)addGUIControl(new InputField());
            addRelationInput.Location = new Vector2(0, 420);

            workTileModeButton.Clicked += new OnClickHandler(doWorkTileButtonClicked);
            buildTileModeButton.Clicked += new OnClickHandler(doBuildTileButtonClicked);
            destroyTileModeButton.Clicked += new OnClickHandler(doDestroyTileButtonClicked);
            craftResourceModeButton.Clicked += new OnClickHandler(doCraftResourceButtonClicked);
            addRelationInput.InputSubmit += new OnInputSubmit(doAddBuildingRelationSubmit);

            if (selectedBuilding.getWorkshopAttribute() == null)
            {
                this.workTileModeButton.Visible = false;
                this.buildTileModeButton.Visible = false;
            }
            if (selectedBuilding.getCraftingAttribute() == null)
            {
                this.craftResourceModeButton.Visible = false;
            }
        }


        //-----------------------------Event Handlers---------------------------------------
        public void doAddBuildingRelationSubmit(object sender, InputSubmitArgs e)
        {
            int p = -1;
            if (int.TryParse(e.data, out p))
            {
                gameController.PlayerCivilization.addBuildingRelation(
                    selectedBuilding.ID, p);
                addRelationInput.clear();
            }
        }

        public void doCraftResourceButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarCraftResource(gameView,
                gameController,
                viewportManager,
                selectedBuilding);
        }

        public void doDestroyTileButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarDestroyTile(gameView,
                gameController,
                viewportManager,
                selectedBuilding);
        }

        public void doWorkTileButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarWorkTile(gameView,
                gameController,
                viewportManager,
                selectedBuilding);
        }

        public void doBuildTileButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarBuild(gameView,
                gameController, viewportManager,
                selectedBuilding);
        }
    }
}
