﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    class SidebarSelectedBuilding : Sidebar
    {
        protected WorldMapBuilding selectedBuilding;

        public SidebarSelectedBuilding(GameView gameView, GameController gameController, 
            ViewportManager viewportManager, WorldMapBuilding selectedBuilding)
            : base(gameView, gameController, viewportManager)
        {
            this.selectedBuilding = selectedBuilding;
            infoPanel.Text = "Not owned\n" + selectedBuilding.ToString();
        }
    }
}
