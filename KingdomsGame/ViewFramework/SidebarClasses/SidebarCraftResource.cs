﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    class SidebarCraftResource : Sidebar
    {
        private WorldMapBuilding selectedBuilding;
        private Button backButton;
        private Button craftButton;
        private Button leftButton;
        private Button rightButton;
        private int selectedItemToCraftIndex;

        public SidebarCraftResource(GameView gameView, GameController gameController,
            ViewportManager viewportManager, WorldMapBuilding selectedBuilding)
            : base(gameView, gameController, viewportManager)
        {
            craftButton = addButton(new Button("Craft", new Vector2(20, 40)));
            backButton = addButton(new Button("Back", new Vector2(20, 20)));
            leftButton = addButton(new Button("<-", new Vector2(20, 100)));
            rightButton = addButton(new Button("->", new Vector2(40, 100)));
            selectedItemToCraftIndex = 0;

            backButton.Clicked += new OnClickHandler(doBackButtonClicked);
            craftButton.Clicked += new OnClickHandler(doCraftButtonClicked);
            rightButton.Clicked += new OnClickHandler(doRightButtonClicked);
            leftButton.Clicked += new OnClickHandler(doLeftButtonClicked);
            this.selectedBuilding = selectedBuilding;
            updateSelectedItemToCraft();
        }

        private void updateSelectedItemToCraft()
        {
            infoPanel.Text = "Craft a :" + getSelectedCraftingItem().Name;
        }

        private CraftingItem getSelectedCraftingItem()
        {
            return selectedBuilding.getCraftingAttribute().
                getItemsThatCanBeCrafted().ElementAt(selectedItemToCraftIndex);
        }

        private void doCraftButtonClicked(object sender, ButtonClickArgs e)
        {
            gameController.PlayerCivilization.addCraftResourceAction(selectedBuilding,
                getSelectedCraftingItem().ItemResource);
        }

        private void doLeftButtonClicked(object sender, ButtonClickArgs e)
        {
            selectedItemToCraftIndex--;

            if (selectedItemToCraftIndex < 0)
                selectedItemToCraftIndex = 0;
        }

        private void doRightButtonClicked(object sender, ButtonClickArgs e)
        {
            selectedItemToCraftIndex++;

            if (selectedItemToCraftIndex >= selectedBuilding.getCraftingAttribute().
                getItemsThatCanBeCrafted().Count())
                selectedItemToCraftIndex--;

        }

        private void doBackButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarSelectedPlayerBuilding(gameView,
                gameController, viewportManager, selectedBuilding);
        }
    }
}
