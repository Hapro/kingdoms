﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace KingdomsGame.ViewFramework
{
    public class SidebarDefault : Sidebar
    {
        protected WorldMapBuilding displayedBuilding;
        protected string tileSelectAction;

        public SidebarDefault(GameView gameView, GameController gameController, 
            ViewportManager viewportManager)
            : base(gameView, gameController, viewportManager)
        {
        }
    }
}
