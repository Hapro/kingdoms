﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    class SidebarBuild : Sidebar
    {
        private WorldMapBuilding selectedBuilding;
        private Button backButton;
        private List<WorldMapBuilding> lstAvailableBuildingsToConstruct;
        private ListBox listBox;

        private string BuildingToConstructName
        {
            get
            {
                return listBox.SelectedItem;
            }
        }

        private WorldMapBuilding BuildingToConstruct
        {
            get
            {
                return gameController.PlayerCivilization.AvailableBuildings
                    [listBox.SelectedIndex];
            }
        }

        public SidebarBuild(GameView gameView, GameController gameController, 
            ViewportManager viewportManager, WorldMapBuilding selectedBuilding)
            : base(gameView, gameController, viewportManager)
        {
            listBox = (ListBox)addGUIControl(new ListBox("Buildings"));
            listBox.Location = new Vector2(0, 300);
            backButton = addButton(new Button("Back", new Vector2(20, 200)));
            backButton = addButton(new Button("Back", new Vector2(20, 20)));
            infoPanel.Text = "Place building in valid location";
            backButton.Clicked += new OnClickHandler(doBackButtonClicked);

            this.selectedBuilding = selectedBuilding;

            lstAvailableBuildingsToConstruct =
                gameController.PlayerCivilization.AvailableBuildings;

            foreach(WorldMapBuilding wmb in gameController.PlayerCivilization.AvailableBuildings)
            {
                listBox.Items.Add(wmb.Name);
            }

            gameView.previewBuilding(BuildingToConstruct.Blueprint);
        }

        private void doListBoxIndexChange(object sender, IndexChangeArgs e)
        {
        }

        private void doBackButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarSelectedPlayerBuilding(gameView,
                gameController, viewportManager, selectedBuilding);
            gameView.stopPreviewingBuilding();
        }

        public override void doWorldMapOverlayTileSelect(object sender, TileSelectArgs e)
        {
            var t = BuildingToConstruct.Blueprint.Tiles;

            //Add all the tiles in the bprint to the game world
            for(int x = 0;  x < t.Width; x++)
            {
                for(int y = 0; y < t.Height; y++)
                {
                    WorldMapTile tileInBlueprint = t.getWorldTileAt(x, y);

                    if (tileInBlueprint != null)
                    {
                        WorldMapTile tileToBuildOn = gameController.WorldMap.
                            getWorldTileAt(e.worldMapTile.X + x, e.worldMapTile.Y + y);

                        gameController.PlayerCivilization.addBuildTileAction(
                        selectedBuilding,
                        tileToBuildOn,
                        tileInBlueprint.TileType);
                    }
                }
            }

            //Only track permamnent buildings
            //-impermanent buildings are things like 
            //farm tiles
            if(BuildingToConstruct.Blueprint.Permanent)
            {
                gameController.PlayerCivilization.
                    addBuilding(gameController.addBuildingToGame(
                    (WorldMapBuilding)BuildingToConstruct.Clone(),
                    e.worldMapTile.X,
                    e.worldMapTile.Y));
            }

            gameView.stopPreviewingBuilding();
        }
    }
}
