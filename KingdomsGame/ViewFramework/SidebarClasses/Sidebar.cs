﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    /// <summary>
    /// Sidebar contains information about currently 
    /// selected buildings, and the actions that can be taken by them
    /// </summary>
    public class Sidebar : GUIComponent
    {
        protected Panel infoPanel;

        public Sidebar(GameView gameView, GameController gameController, 
            ViewportManager viewportManager)
            : base(gameView, gameController, viewportManager)
        {
            this.infoPanel = new Panel(180, 0);

            addGUIControl(infoPanel);
            scaleGUIToViewport();
        }

        public override void scaleGUIToViewport()
        {
            infoPanel.Height = (int)viewportManager.VirtualViewportHeight;
            infoPanel.Location = new Vector2(0, 0);
            infoPanel.Text = "";
        }

        public virtual void clearPanelText()
        {
            infoPanel.Text = "";
        }
        
        //------------------------Event handlers--------------------------------------

        public virtual void doWorldMapOverlayBuildingSelect(object sender, BuildingSelectArgs e)
        {
            if (!e.isPlayerOwned)
            {
                gameView.Sidebar = new SidebarSelectedBuilding(gameView,
                    gameController, viewportManager, e.worldMapBuilding);
            }
            else
            {
                gameView.Sidebar = new SidebarSelectedPlayerBuilding(gameView,
                    gameController, viewportManager, e.worldMapBuilding);
            }
        }

        public virtual void doWorldMapOverlayTileSelect(object sender, TileSelectArgs e)
        {
            gameView.Sidebar = new SidebarDefault(gameView,
                gameController, viewportManager);
        }
    }
}
