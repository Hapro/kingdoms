﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace KingdomsGame.ViewFramework.SidebarClasses
{
    class SidebarCivManager : Sidebar
    {
        private ListBox lstBoxCharactersInCiv;
        private ListBox lstBoxWorkBuilding;
        private ListBox lstBoxHomeBuilding;
        private Button btnUpdate;
        private Button btnBack;

        public SidebarCivManager(GameView gameView, GameController gameController, 
            ViewportManager viewportManager)
            : base(gameView, gameController, viewportManager)
        {
            lstBoxCharactersInCiv = (ListBox)addGUIControl(new ListBox("Character:"), 0, 1);
            lstBoxWorkBuilding = (ListBox)addGUIControl(new ListBox("Current workplace"), 0, 3);
            lstBoxHomeBuilding = (ListBox)addGUIControl(new ListBox("Current home"), 0, 5);
            btnUpdate = (Button)addGUIControl(new Button("Confirm"), 0, 7);
            btnBack = (Button)addGUIControl(new Button("Back"), 0, 8);
            
            foreach(int i in gameController.PlayerCivilization.CharacterIDs)
            {
                lstBoxCharactersInCiv.Items.Add(i.ToString());
            }

            lstBoxCharactersInCiv.onIndexChange += new OnIndexChange(doSelectedCharacterChanged);
            btnUpdate.Clicked += new OnClickHandler(doConfirmCharacterUpdateClicked);
            btnBack.Clicked += new OnClickHandler(doBackButtonClicked);
        }

        private WorldMapHuman getSelectedWorldMapHuman()
        {
            return (WorldMapHuman)gameController.WorldMap.getWorldMapCharacterById(
                int.Parse(lstBoxCharactersInCiv.SelectedItem));
        }

        private void doBackButtonClicked(object sender, ButtonClickArgs e)
        {
            gameView.Sidebar = new SidebarDefault(gameView,
                gameController,
                viewportManager);
        }

        private void doConfirmCharacterUpdateClicked(object sender, ButtonClickArgs e)
        {
            WorldMapHuman wmh = getSelectedWorldMapHuman();

            if (wmh.IsIdle) //only update work and home buildings if idle
            {
                wmh.WorkBuilding = gameController.WorldMap.getBuildingById
                    (int.Parse(lstBoxWorkBuilding.SelectedItem));

                wmh.HomeBuilding = gameController.WorldMap.getBuildingById
                    (int.Parse(lstBoxHomeBuilding.SelectedItem));

                wmh.Script = "relocate"; //tell character to go to new home/work
                wmh.State = "ready";
            }
        }

        private void doSelectedCharacterChanged(object sender, IndexChangeArgs e)
        {
            WorldMapHuman wmh;

            foreach(int i in gameController.PlayerCivilization.BuildingIDs)
            {
                if(gameController.WorldMap.getBuildingById(i).hasAttribute(BuildingAttributes.HOME))
                    lstBoxHomeBuilding.Items.Add(i.ToString());

                lstBoxWorkBuilding.Items.Add(i.ToString());
            }

            wmh = getSelectedWorldMapHuman();

            lstBoxHomeBuilding.setIndexToMatch(wmh.HomeBuilding.ID.ToString());
            lstBoxWorkBuilding.setIndexToMatch(wmh.WorkBuilding.ID.ToString());
        }
    }
}
