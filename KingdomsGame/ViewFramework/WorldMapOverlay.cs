﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    public class WorldMapOverlay
    {
        public OnTileSelectHandler TileSelected;
        public OnBuildingSelectHandler BuildingSelected;

        private WorldMapTile selectedTile = null;
        private WorldMapBuilding selectedBuilding = null;
        private GameController gameController;
        private Rectangle clickableArea;
        private Texture2D tileTexture;
        private WorldMapBuildingBlueprint buildingPreview = null;
        private Vector2 mouseLoc;

        public Rectangle ClickableArea
        {
            get { return clickableArea; }
            set { clickableArea = value; }
        }

        public WorldMapBuilding SelectedBuilding
        {
            get { return selectedBuilding; }
        }

        public WorldMapOverlay(GameController gameController)
        {
            this.gameController = gameController;
        }

        public void loadContent(ContentManager content)
        {
            tileTexture = content.Load<Texture2D>("tile");
        }

        public void clicked(UserInput userInput)
        {
            if (clickableArea.Contains(mouseLoc))
            {
                Vector2 translatedLoc =
                    gameController.WorldMap.translateLocalLocToGlobalLoc(mouseLoc /
                    WorldMap.TILE_SIZE);

                foreach (WorldMapBuilding wmb in gameController.WorldMap.getAllWorldMapBuildings())
                {
                    if (wmb.Boundary.Contains(translatedLoc))
                    {
                        selectedBuilding = wmb;
                        BuildingSelected(this,
                            new BuildingSelectArgs(wmb,
                                gameController.PlayerCivilization.BuildingIDs.Contains(wmb.ID)));
                    }
                }

                WorldMapTile wmt = gameController.WorldMap.getWorldTileAt(
                    (int)translatedLoc.X,
                    (int)translatedLoc.Y);

                if (wmt != null && selectedBuilding != null &&
                    !selectedBuilding.Boundary.Contains(wmt.X, wmt.Y))
                    TileSelected(this,
                        new TileSelectArgs(wmt));
            }
        }

        public void update(UserInput userInput)
        {
            mouseLoc = userInput.MouseLoc;

            if (userInput.LeftClick)
            {
                clicked(userInput);
            }

        }

        public void setBuildingPreview(WorldMapBuildingBlueprint wmbb)
        {
            this.buildingPreview = wmbb;
        }

        public void unsetBuildingPreview()
        {
            this.buildingPreview = null;
        }

        public void draw(SpriteBatch sb)
        {
            if(this.buildingPreview != null)
            {
                for(int x = 0; x < buildingPreview.BuildingBoundary.Width; x++)
                {
                    for(int y = 0; y < buildingPreview.BuildingBoundary.Height; y++)
                    {
                       WorldMapTile wmt = buildingPreview.Tiles.getWorldTileAt(x, y);
                        sb.Draw(tileTexture,
                            (new Vector2(wmt.X, wmt.Y) * WorldMap.TILE_SIZE)
                            + mouseLoc, 
                            Color.Red * 0.4f);
                    }
                }
            }

            foreach (int i in gameController.PlayerCivilization.BuildingIDs)
            {
                WorldMapBuilding wmb = gameController.WorldMap.getBuildingById(i);

                //if building is visible and incomplete
                if (!wmb.checkBuildingIntegrity(gameController.WorldMap))
                {
                    sb.Draw(tileTexture, gameController.WorldMap.translateGlobalLocToLocalLoc(wmb.Location)
                        * WorldMap.TILE_SIZE, Color.Red * 0.4f);
                    //Draw highlighted area for where building will be
                }
            }
        }
    }
}
