﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KingdomsGame.ViewFramework
{
    public class ListBox : GUIControl
    {
        private List<string> items;
        private int index;
        private Button left;
        private Button right;
        private Panel label;

        public OnIndexChange onIndexChange;

        public int SelectedIndex
        {
            get { return index; }
        }

        public string SelectedItem
        {
            get { return items[index]; }
        }

        public List<string> Items
        {
            get { return items; }
            set { items = value; }
        }

        public ListBox(string labelText)
            :base()
        {
            items = new List<string>();
            index = 0;

            left = new Button("<-", new Vector2(0, 20));
            right = new Button("->", new Vector2(20, 20));
            label = new Panel(0, 0);
            label.Location = new Vector2(0, -20);

            label.Text = labelText;

            addChildControl(left);
            addChildControl(right);
            addChildControl(label);

            left.Clicked += new OnClickHandler(doLeftClicked);
            right.Clicked += new OnClickHandler(doRightClicked);
            onIndexChange += new OnIndexChange(doIndexChange);
        }

        public void setIndexToMatch(string s)
        {
            for(int i = 0; i < items.Count; i++)
            {
                if(items[i] == s)
                {
                    int change = index - i;
                    onIndexChange(this, new IndexChangeArgs(change));
                    break;
                }
                    
            }
        }

        private void doIndexChange(object sender, IndexChangeArgs e)
        {
            this.text = SelectedItem;
        }

        private void doLeftClicked(object sender, ButtonClickArgs e)
        {
            index--;
            if (index < 0)
                index = 0;

            onIndexChange(this, new IndexChangeArgs(1));
        }

        private void doRightClicked(object sender, ButtonClickArgs e)
        {
            index++;

            if (index >= items.Count)
                index--;

            onIndexChange(this, new IndexChangeArgs(1));
        }
    }
}