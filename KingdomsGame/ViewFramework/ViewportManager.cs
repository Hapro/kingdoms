﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace KingdomsGame.ViewFramework
{
    public class ViewportManager
    {
        private Vector2 viewportScale;
        private Matrix viewportTransform;
        private float virtualViewportWidth;
        private float virtualViewportHeight;
        private GraphicsDeviceManager graphics;

        public OnResolutionChange ResolutionChanged;

        public Matrix ViewportTransform { get { return viewportTransform; } }
        public Vector2 ViewportScale { get { return viewportScale; } }

        public float VirtualViewportWidth
        {
            get { return virtualViewportWidth; }
        }
        public float VirtualViewportHeight
        {
            get { return virtualViewportHeight; }
        }

        public ViewportManager(GraphicsDeviceManager graphics)
        {
            this.graphics = graphics;
        }

        public void updateScreenResoltuion(int physicalWidth, int physicalHeight, 
            bool widescreen, bool fullscreen)
        {
            setWidescreen(widescreen);

            //Sets size of window
            graphics.PreferredBackBufferWidth = physicalWidth;
            graphics.PreferredBackBufferHeight = physicalHeight;

            //Set fullscreen or not
            graphics.IsFullScreen = fullscreen;

            //find the scale factor for the size we are using for game co-ords vs the resolution
            //of our users screen
            viewportScale = new Vector2(physicalWidth / virtualViewportWidth,
                                        physicalHeight / virtualViewportHeight);

            //create the transform to pass into draw so that we are scaling correctly
            viewportTransform = Matrix.CreateScale(physicalWidth / virtualViewportWidth,
                physicalHeight / virtualViewportHeight,
                1);

            graphics.ApplyChanges();
            ResolutionChanged(this, EventArgs.Empty);
        }

        private void setWidescreen(bool widescreen)
        {
            if (widescreen) //16:9
            {
                virtualViewportWidth = 1280;
                virtualViewportHeight = 720;
            }
            else //4:3
            {
                virtualViewportWidth = 1280;
                virtualViewportHeight = 960;
            }
        }

    }
}
