﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    public abstract class GUIControl
    {
        protected Texture2D texture;
        protected SpriteFont font;
        protected string text;
        protected Vector2 location;
        protected Color color;
        protected bool visible;
        protected List<GUIControl> childControls;

        public Texture2D Texture { get { return texture; } set { texture = value; } }
        public SpriteFont Font { get { return font; } set { font = value; } }
        public string Text { get { return text; } set { text = value; } }
        public Vector2 Location { get { return location; } set { location = value; } }
        public bool Visible { get { return visible; } set { visible = value; } }


        public OnClickHandler Clicked;

        public virtual Rectangle Bounds
        {
            get
            {
                Vector2 d = font.MeasureString(text);
                return new Rectangle(
                    (int)location.X,
                    (int)location.Y,
                    (int)d.X,
                    (int)d.Y);
            }
        }

        protected virtual Vector2 TextLoc
        {
            get { return location; }
        }

        public GUIControl()
        {
            this.color = Color.White;
            this.visible = true;
            Clicked = delegate { };
            this.childControls = new List<GUIControl>();
            this.text = "";

        }

        public virtual void loadContent(ContentManager c)
        {
            foreach (GUIControl gc in childControls)
                gc.loadContent(c);

            Texture = c.Load<Texture2D>("buttonImg");
            Font = c.Load<SpriteFont>("buttonFont");
        }

        public void addChildControl(GUIControl gc)
        {
            this.childControls.Add(gc);
        }

        public virtual void update(UserInput userInput)
        {
            foreach (GUIControl gc in childControls)
            {
                gc.location += location;
                gc.update(userInput);
                gc.location -= location;
            }

            if (visible && userInput.LeftClick && Bounds.Contains(userInput.MouseLoc))
            {
                Clicked(this, new ButtonClickArgs());
            }
        }

        public virtual void draw(SpriteBatch sb)
        {
            if (visible)
            {
                sb.Draw(texture, Bounds, color);
                sb.DrawString(font, text, TextLoc, Color.Black);
            }

            foreach(GUIControl gc in childControls)
            {
                gc.location += location;
                gc.draw(sb);
                gc.location -= location;
            }
        }
    }
}
