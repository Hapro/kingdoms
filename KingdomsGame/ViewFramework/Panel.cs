﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace KingdomsGame.ViewFramework
{
    public class Panel : GUIControl
    {
        private int width;
        private int height;

        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }

        public override Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    (int)location.X,
                    (int)location.Y,
                    width,
                    height);
            }
        }

        public Panel(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
    }
}
