﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoGame.Framework;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace KingdomsGame.ViewFramework
{
    public class GameView
    {
        private GameController gameController;
        private UserInput userInput;
        private WorldMapOverlay worldMapOverlay;
        private ViewportManager viewportManager;
        private Sidebar sidebar;
        private Sidebar nextSidebar;
        private ContentManager content;

        public Sidebar Sidebar
        {
            get { return this.sidebar; }
            set { nextSidebar = value; }
        }

        public GameView(GameController gameController, ViewportManager viewportManager)
        {
            this.viewportManager = viewportManager;
            this.gameController = gameController;
            this.sidebar = new SidebarDefault(this, gameController, viewportManager);
            this.nextSidebar = sidebar;
            this.userInput = new UserInput(viewportManager.ViewportScale);
            this.worldMapOverlay = new WorldMapOverlay(gameController);

            this.viewportManager.ResolutionChanged +=
                new OnResolutionChange(userInput.doViewportManagerResolutionChange);
            this.viewportManager.ResolutionChanged +=
                new OnResolutionChange(doViewportManagerResolutionChange);

            registerEventsToSidebar();
        }

        private void unregisterEventsFromSidebar()
        {
            this.viewportManager.ResolutionChanged -=
                new OnResolutionChange(sidebar.doViewportManagerResolutionChange);
            this.worldMapOverlay.BuildingSelected -=
                new OnBuildingSelectHandler(sidebar.doWorldMapOverlayBuildingSelect);
            this.worldMapOverlay.TileSelected -=
                new OnTileSelectHandler(sidebar.doWorldMapOverlayTileSelect);
        }

        private void registerEventsToSidebar()
        {
            this.viewportManager.ResolutionChanged +=
                new OnResolutionChange(sidebar.doViewportManagerResolutionChange);
            this.worldMapOverlay.BuildingSelected +=
                new OnBuildingSelectHandler(sidebar.doWorldMapOverlayBuildingSelect);
            this.worldMapOverlay.TileSelected +=
                new OnTileSelectHandler(sidebar.doWorldMapOverlayTileSelect);
        }

        private void doViewportManagerResolutionChange(object sender, EventArgs e)
        {
            scaleGUIToViewport();
        }

        public void scaleGUIToViewport()
        {
            this.worldMapOverlay.ClickableArea = new Rectangle(
                180,
                0,
                (int)viewportManager.VirtualViewportWidth - 180,
                (int)viewportManager.VirtualViewportHeight);
        }

        public void loadContent(ContentManager content)
        {
            sidebar.loadContent(content);
            worldMapOverlay.loadContent(content);
            this.content = content;
        }

        public void update(float elapsed)
        {
            userInput.update();
            worldMapOverlay.update(userInput);
            sidebar.update(userInput);

            if (userInput.keyPressed(Keys.Left))
                gameController.WorldMap.moveCamera(-1, 0);

            if (userInput.keyPressed(Keys.Right))
                gameController.WorldMap.moveCamera(1, 0);

            if (userInput.keyPressed(Keys.Up))
                gameController.WorldMap.moveCamera(0, -1);

            if (userInput.keyPressed(Keys.Down))
                gameController.WorldMap.moveCamera(0, 1);

            if (userInput.keyPressed(Keys.Escape))
                gameController.exitGame();

            if(userInput.keyPressed(Keys.E))
            {
                nextSidebar = new SidebarClasses.SidebarCivManager(this,
                    gameController,
                    viewportManager);
            }

            if(nextSidebar != sidebar)
            {
                unregisterEventsFromSidebar();
                sidebar = nextSidebar;
                sidebar.loadContent(content);
                registerEventsToSidebar();
            }
        }


        public void previewBuilding(WorldMapBuildingBlueprint wmbb)
        {
            worldMapOverlay.setBuildingPreview(wmbb);
        }

        public void stopPreviewingBuilding()
        {
            worldMapOverlay.unsetBuildingPreview();
        }

        public void draw(SpriteBatch sb)
        {
            worldMapOverlay.draw(sb);
            sidebar.draw(sb);
        }
    }
}
