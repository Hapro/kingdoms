﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame.ViewFramework
{
    public delegate void OnTileSelectHandler(object sender, TileSelectArgs e);
    public delegate void OnBuildingSelectHandler(object sender, BuildingSelectArgs e);
    public delegate void OnClickHandler(object sender, ButtonClickArgs e);
    public delegate void OnResolutionChange(object sender, EventArgs e);
    public delegate void OnIndexChange(object sender, IndexChangeArgs e);
    public delegate void OnInputSubmit(object sender, InputSubmitArgs e);

    public class InputSubmitArgs : EventArgs
    {
        public string data;

        public InputSubmitArgs(string data)
        {
            this.data = data;
        }
    }

    public class IndexChangeArgs : EventArgs
    {
        public int change;

        public IndexChangeArgs(int change)
        {
            this.change = change;
        }
    }

    public class BuildingSelectArgs : EventArgs
    {
        public WorldMapBuilding worldMapBuilding;
        public bool isPlayerOwned;

        public BuildingSelectArgs(WorldMapBuilding worldMapBuilding,
            bool isPlayerOwned)
        {
            this.isPlayerOwned = isPlayerOwned;
            this.worldMapBuilding = worldMapBuilding;
        }
    }

    public class TileSelectArgs : EventArgs
    {
        public WorldMapTile worldMapTile;

        public TileSelectArgs(WorldMapTile worldMapTile)
        {
            this.worldMapTile = worldMapTile;
        }
    }

    public class ButtonClickArgs : EventArgs
    {

    }
}
