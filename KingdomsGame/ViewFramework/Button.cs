﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KingdomsGame.ViewFramework
{
    public class Button : GUIControl
    {
        public override Rectangle Bounds
        {
            get
            {
                Vector2 d = font.MeasureString(text);
                return new Rectangle(
                    (int)location.X,
                    (int)location.Y,
                    (int)d.X,
                    (int)d.Y);
            }
        }

        public Button(string text, Vector2 location)
            :base()
        {
            this.location = location;
            this.text = text;
        }

        public Button(string text)
            : base()
        {
            this.text = text;
        }

        public override void update(UserInput userInput)
        {
            if (Bounds.Contains(userInput.MouseLoc))
                this.color = Color.DarkSeaGreen;
            else
                this.color = Color.White;

            base.update(userInput);
        }
    }
}
