﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using MoonSharp.Interpreter;

namespace KingdomsGame
{
    [MoonSharpUserData]
    public class WorldMapTile : ICloneable
    {
        private ColorGraph colorType;
        private TileType tileType;
        private int x = -1;
        private int y = -1;
        private bool passable;
        private WorldMapCharacter character;
        private Color color;

        public TileType TileType { get { return tileType; } set { tileType = value; } }
        public int X { get { return x; } }
        public int Y { get { return y; } }
        public bool Passable { get { return passable; } set { passable = value; } }
        public WorldMapCharacter Character { get { return character; } set { character = value; } }
        public Color Color { get { return color; } set { color = value; } }
        public ColorGraph ColorType { get { return colorType; } set { colorType = value; } }

        public WorldMapTile(TileType tileType, bool passable)
        {
            this.passable = passable;
            this.tileType = tileType;
            this.character = null;
            this.color = Color.White;
            this.colorType = ColorGraph.NONE;
        }

        public WorldMapTile(TileType tileType, bool passable, Color color, ColorGraph colorType)
        {
            this.passable = passable;
            this.tileType = tileType;
            this.character = null;
            this.color = color;
            this.colorType = colorType;
        }

        public void setLocation(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public object Clone()
        {
            WorldMapTile wmt = new WorldMapTile(this.tileType,
                this.passable,
                color,
                colorType);
            wmt.x = this.x;
            wmt.y = this.y;
            wmt.colorType = this.colorType;
            wmt.color = this.color;
            return wmt;
        }
    }
}
