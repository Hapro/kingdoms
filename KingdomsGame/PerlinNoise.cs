﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    public class PerlinNoise
    {
        private double persistance;
        private double initialAmplitude;
        private double initialFrequency;
        private int width;
        private int height;
        private Grid<double> grid;

        public PerlinNoise(double persistance,
            double initialAmplitude, double initialFrequency)
        {
            this.persistance = persistance;
            this.initialAmplitude = initialAmplitude;
            this.initialFrequency = initialFrequency;
        }

        /// <summary>
        /// Call before using to generate a new grid
        /// </summary>
        /// <param name="width">width of noise</param>
        /// <param name="height">height of noise</param>
        public void generate(int width, int height)
        {
            this.grid = getGridFilledWithRandoms(width, height);
            this.width = width;
            this.height = height;
        }

        private uint randomNumber(uint seed)
        {
            uint m_z = 36969 * (seed & 65535) + (seed >> 16);
            uint m_w = 18000 * (seed & 65535) + (seed >> 16);
            return (m_z << 16) + m_w;
        }

        public double getUniform(uint seed)
        {
            // 0 <= u < 2^32
            uint u = randomNumber(seed);
            // The magic number below is 1/(2^32 + 2).
            // The result is strictly between 0 and 1.
            return (u + 1.0) * 2.328306435454494e-10;
        }

        private double noise(double x, double y)
        {
            return grid.getItemAt((int)x, (int)y);
        }

        /// <summary>
        /// cosine interpolation
        /// </summary>
        private double lerp(double a, double b, double prop)
        {
            double ft = prop * 3.1415927f;
            double f = (1 - Math.Cos(ft)) * 0.5f;

            return a * (1 - f) + b * f;
        }

        /// <summary>
        /// Interpolate between 2 values on the function
        /// to get the value at that location
        /// </summary>
        /// <param name="x">x location to get</param>
        /// <returns></returns>
        private double valueAt(double x, double y)
        {
            //calculate noise at each point
            double v1 = noise(x, y);
            double v2 = noise(x + 1, y);
            double v3 = noise(x, y + 1);
            double v4 = noise(x + 1, y + 1);

            double fractionalX = x - Math.Floor(x);
            double fractionalY = y - Math.Floor(y);

            //interpolate between the points
            double i1 = lerp(v1, v2, fractionalX); //can replace 0 with fractional x and y
            double i2 = lerp(v3, v4, fractionalX);
            return lerp(i1, i2, fractionalY);
        }

        /// <summary>
        /// Must call generate() first
        /// </summary>
        /// <param name="octaves">octaves determines how 'detailed' the noise is</param>
        /// <returns></returns>
        public Grid<double> getPerlinNoise(int octaves)
        {
            Grid<double> noise = new Grid<double>(grid.Width, height);
            octaves = 6;
            double frequency = initialFrequency;
            double amplitude = initialAmplitude;

            for (int i = 0; i < octaves; i++)
            {
                frequency = Math.Pow(2, i) * 0.1;
                amplitude = amplitude * persistance;

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        double originalVal = noise.getItemAt(x, y);
                        double value =
                            valueAt(
                            (double)x * frequency,
                            (double)y * frequency)
                            * amplitude;

                        noise.addItemAt(originalVal + value, x, y);
                    }
                }
            }

            return noise;
        }

        private Grid<double> getGridFilledWithRandoms(int width, int height)
        {
            Grid<double> grid = new Grid<double>(width, height);
            Random rand = new Random(23);

            double min = 100;
            double max = 0;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    double v = rand.NextDouble();

                    if (v > max)
                        max = v;

                    if (v < min)
                        min = v;

                    grid.addItemAt(rand.NextDouble(), x, y);
                }
            }
            return grid;
        }
    }
}
