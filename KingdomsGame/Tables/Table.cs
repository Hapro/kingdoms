﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame.Tables
{
    public abstract class TableEntry
    {
        protected bool finished = false;

        public bool Finished { get { return finished; } set { finished = value; } }
    }

    public abstract class Table
    {
        private List<TableEntry> lstEntries;

        public Table()
        {
            lstEntries = new List<TableEntry>();
        }

        public IEnumerable<TableEntry> getTableEntries()
        {
            foreach(TableEntry te in lstEntries)
                yield return te;
        }

        public virtual IEnumerable<TableEntry> query(Func<TableEntry, bool> query)
        {
            foreach (TableEntry te in getTableEntries())
                if (query(te)) yield return te;
        }

        protected virtual void addEntry(TableEntry tableEntry)
        {
            this.lstEntries.Add(tableEntry);
        }

        protected virtual void removeEntry(TableEntry tableEntry)
        {
            this.lstEntries.Remove(tableEntry);
        }

        public virtual void purge()
        {
            List<TableEntry> newLst = new List<TableEntry>(lstEntries.Count);

            for(int i = 0; i < lstEntries.Count; i++)
            {
                if (!lstEntries[i].Finished)
                    newLst.Add(lstEntries[i]);
            }

            lstEntries = newLst;
        }
    }
}
