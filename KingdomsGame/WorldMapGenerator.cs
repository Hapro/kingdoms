﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KingdomsGame.Factories;
using Microsoft.Xna.Framework;

namespace KingdomsGame
{
    public class WorldMapGenerator
    {
        private TileFactory tileFactory;
        private Random random;
        private PerlinNoise perlinNoise;

        public WorldMapGenerator()
        {
            tileFactory = new TileFactory();
            random = new Random();
            perlinNoise = new PerlinNoise(0.5,1, 1);
        }

        public WorldMap generateMap(int width, int height)
        {
            Grid<double> noise;
            WorldMap worldMap = new WorldMap(width, height);

            clearWorldMap(worldMap, TileType.WATER);
            
            perlinNoise.generate(width, height);
            noise = perlinNoise.getPerlinNoise(2);

            worldMap = new WorldMap(width, height);
            worldMap.HeightMap = noise;

            for (int x = 0; x < noise.Width; x++)
            {
                for(int y= 0; y < noise.Height; y++)
                {
                    float val = (float)noise.getItemAt(x, y);
                    WorldMapTile wmt = createTileBasedOnNoiseValue(val);
                    worldMap.addWorldTileAt(wmt, null, x, y);
                }
            }
            return worldMap;
        }


        private WorldMapTile createTileBasedOnNoiseValue(double value)
        {
            WorldMapTile wmt;

            if (value < 0.4)
            {
                wmt = (WorldMapTile)tileFactory.create(TileType.WATER);
            }
            else if(value > 0.6)
            {
                wmt = (WorldMapTile)tileFactory.create(TileType.TREE);
            }
            else
            {
                wmt = (WorldMapTile)tileFactory.create(TileType.GRASS);
            }
           
            return wmt;
        }

        private void clearWorldMap(WorldMap worldMap, TileType tileToFill)
        {
            for(int x = 0; x < worldMap.Width; x++)
            {
                for(int y = 0;  y <worldMap.Height; y++)
                {
                    worldMap.addWorldTileAt((WorldMapTile)tileFactory.create(tileToFill), null, x, y);
                }
            }
        }
    }
}
