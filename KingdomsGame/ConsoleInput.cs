﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Loaders;
using System.IO;

namespace KingdomsGame
{
    public class ConsoleInput
    {
        private GameController gameController;

        public ConsoleInput(GameController gameController)
        {
            this.gameController = gameController;
        }

        public void pollInput()
        {
            string s = "";

            do
            {
                s = Console.ReadLine();

                try
                {
                    gameController.ScriptRunner.executeScript(s);
                }
                catch (InterpreterException e)
                {
                    Console.WriteLine(e.DecoratedMessage);
                }

            } while (true);
        }
    }
}
