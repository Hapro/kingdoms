﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace KingdomsGame
{
    public class ColorGradientGraph : ICloneable
    {
        //<x-co-ord, color>
        List<KeyValuePair<double, Color>> lstColorSample;

        public List<KeyValuePair<double, Color>> ColorSamples
        {
            get { return lstColorSample; }
            set { lstColorSample = value; }
        }

        public ColorGradientGraph()
        {
            lstColorSample = new List<KeyValuePair<double, Color>>();
            lstColorSample.Add(new KeyValuePair<double, Color>(0.0, Color.White));
        }

        /// <summary>
        /// Get color at x-cord
        /// </summary>
        /// <param name="x">between 0 and 100</param>
        /// <returns></returns>
        public Color getColorAt(double x)
        {
            KeyValuePair<double, Color> c1 = lstColorSample[0];
            KeyValuePair<double, Color> c2 = lstColorSample[0];
            double amount = 0;

            foreach (KeyValuePair<double, Color> kvp in lstColorSample)
            {
                if (kvp.Key <= x)
                    c1 = kvp;

                if (kvp.Key >= x)
                {
                    c2 = kvp;
                    break;
                }
            }

            double diff = c2.Key - c1.Key;
            double transX = x - c1.Key;

            amount = transX != 0 ? (transX / diff) : 0;

            return Color.Lerp(c1.Value, c2.Value, (float)amount);
        }

        public object Clone()
        {
            ColorGradientGraph cgg = new ColorGradientGraph();
            cgg.lstColorSample = lstColorSample;
            return cgg;
        }
    }

}
