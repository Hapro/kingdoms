﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoGame.Framework;
using KingdomsGame.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KingdomsGame
{
    public class WorldMap
    {
        private WorldMapTiles worldMapTiles;
        private PathFinder pathFinder;
        private WorldMapCharacters worldMapCharacters;
        private WorldMapBuildings worldMapBuildings;
        private Rectangle drawableBounds;
        private Vector2 cameraLoc;
        private EntityMap<TileType, Texture2D> tileTextureMap;
        private List<TileChangeTimer> lstTileChangeTimers;
        private TileFactory tileFactory;
        private ColorGradientGraphFactory colorGradientGraphFactory;
        private Grid<double> heightMap;

        public const int TILE_SIZE = 30;

        public int Width { get { return worldMapTiles.Width; } }
        public int Height { get { return worldMapTiles.Height; } }
        public EntityMap<TileType, Texture2D> TileTextureMap
        {
            get { return tileTextureMap; }
            set { tileTextureMap = value; }
        }
        public Grid<double> HeightMap
        { get { return heightMap; } set { heightMap = value; } }

        public WorldMap(int width, int height)
        {
            worldMapTiles = new WorldMapTiles(width, height);
            pathFinder = new PathFinder(worldMapTiles);
            worldMapCharacters = new WorldMapCharacters();
            worldMapBuildings = new WorldMapBuildings();
            cameraLoc = new Vector2(0, 0);
            drawableBounds = new Rectangle(0, 0, 90, 30);
            lstTileChangeTimers = new List<TileChangeTimer>();
            tileFactory = new TileFactory();
            heightMap = new Grid<double>(width, height);
            colorGradientGraphFactory = new ColorGradientGraphFactory();
        }

        public void moveCamera(Vector2 dir)
        {
            moveCamera((int)dir.X, (int)dir.Y);
        }

        public void moveCamera(int xDir, int yDir)
        {
            cameraLoc.X += xDir;
            cameraLoc.Y += yDir;

            if (cameraLoc.X < 0)
                cameraLoc.X = 0;

            if (cameraLoc.Y < 0)
                cameraLoc.Y = 0;

            if (cameraLoc.Y + drawableBounds.Height > worldMapTiles.Height)
                cameraLoc.Y -= yDir;

            if (cameraLoc.X + drawableBounds.Width > worldMapTiles.Width)
                cameraLoc.X -= xDir;
        }

        /// <summary>
        /// Translates x and y GRID co-ordinates (local loc / tile size)
        /// </summary>
        /// <param name="localLoc">Local Grid co-ordinate</param>
        /// <returns></returns>
        public Vector2 translateLocalLocToGlobalLoc(Vector2 localLoc)
        {
            return localLoc + cameraLoc;
        }

        /// <summary>
        /// Translates x and y GRID co-ordinates (global loc / tile size)
        /// </summary>
        /// <param name="localLoc">Local Grid co-ordinate</param>
        /// <returns></returns>
        public Vector2 translateGlobalLocToLocalLoc(Vector2 localLoc)
        {
            return localLoc - cameraLoc;
        }

        public int addBuildingToWorldMap(WorldMapBuilding wmb, int x, int y)
        {
            int id = worldMapBuildings.addBuilding(wmb);
            wmb.Location = new Vector2(x, y);
            return id;
        }

        public WorldMapBuilding getBuildingById(int id)
        {
            return worldMapBuildings.getBuilding(id);
        }

        public void removeBuildingById(int id)
        {
            worldMapBuildings.removeBuilding(id);
        }

        public void removeCharacterById(int id)
        {
            worldMapCharacters.removeCharacter(id);
        }

        /// <summary>
        /// Adds the supplied world map tile to the given location
        /// Will replace tile if one already exists
        /// </summary>
        /// <param name="wmt">tile to replace</param>
        /// <param name="x">x location</param>
        /// <param name="y">y location</param>
        public void addWorldTileAt(WorldMapTile wmt, TileChangeTimer tileChangeTimer, int x, int y)
        {
            //set tile to correct color
            ColorGradientGraph cgg = (ColorGradientGraph)
                colorGradientGraphFactory.create(wmt.ColorType);
            wmt.Color = cgg.getColorAt(heightMap.getItemAt(x, y));
            worldMapTiles.addWorldTileAt(wmt, x, y);

            if(tileChangeTimer != null)
                this.lstTileChangeTimers.Add(tileChangeTimer);
        }

        /// <summary>
        /// Adds a character to the game world at supplied posiiton
        /// </summary>
        /// <returns>ID of character</returns>
        public int addCharacterToWorldMap(int x, int y)
        {
            WorldMapCharacter wmc = new WorldMapHuman();
            int id = worldMapCharacters.addCharacter(wmc);
            WorldMapTile tile = worldMapTiles.getWorldTileAt(x, y);
            
            if (tile != null)
            {
                putCharacterOnTile(wmc, tile);
                return id;
            }

            return -1;
        }

        /// <summary>
        /// Adds supplied character to the game world at supplied posiiton
        /// </summary>
        /// <returns>ID of character</returns>
        public int addCharacterToWorldMap(WorldMapCharacter wmc, int x, int y)
        {
            int id = worldMapCharacters.addCharacter(wmc);
            putCharacterOnTile(wmc, worldMapTiles.getWorldTileAt(x, y));
            return id;
        }

        /// <summary>
        /// Put the specified character on the specified tile
        /// </summary>
        /// <param name="character">Character</param>
        /// <param name="tile">Tile character wants to be on</param>
        public void putCharacterOnTile(WorldMapCharacter character, WorldMapTile tile)
        {
            if (character.ParentTile != null)
                character.ParentTile.Character = null;

            character.ParentTile = tile;
            tile.Character = character;
        }

        /// <summary>
        /// Finds a path between the supplied x and y co-ordinates
        /// </summary>
        public List<Vector2> findPath(int x1, int y1, int x2, int y2)
        {
            return pathFinder.findPath(x1, y1, x2, y2);
        }

         /// <summary>
        /// Returns true if a character is standing on this tile
        /// </summary>
        /// <param name="wmt">World map tile to check</param>
        /// <returns>True if a character is standing on the tile</returns>
        public bool doesTileHaveCharacter(WorldMapTile wmt)
        {
            return wmt.Character != null;
        }

        /// <summary>
        /// Gets a specific tile at a given location
        /// </summary>
        /// <param name="x">x location</param>
        /// <param name="y">y locaiton</param>
        /// <returns></returns>
        public WorldMapTile getWorldTileAt(int x, int y)
        {
            return worldMapTiles.getWorldTileAt(x, y);
        }

        public List<WorldMapTile> getSquareAdjacentTiles(int x, int y)
        {
            return worldMapTiles.getSquareAdjacentTiles(x, y);
        }

        public List<WorldMapTile> getCrossAdjacentTiles(int x, int y)
        {
            return worldMapTiles.getCrossAdjacentTiles(x, y);
        }

        public bool getAreaPassable(int x, int y, int width, int height)
        {
            bool val = true;

            for(int x1 = x; x1 < (x + width); x1++)
            {
                for (int y1 = y; y1 < (y + height); y1++)
                {
                    WorldMapTile wmt = getWorldTileAt(x1, y1);
                    if (wmt != null)
                    {
                        if (!wmt.Passable) val = false;
                    }
                }
            }

            return val;
        }

        /// <summary>
        /// Once a building is 'registered' to world map,
        /// we can fill in the tiles it needs 
        /// </summary>
        /// <param name="bID"></param>
        public void fillInBuilding(int bID)
        {
            WorldMapBuilding building = getBuildingById(bID);

            for(int x= 0; x <building.Boundary.Width; x++)
            {
                for(int y =0 ; y<building.Boundary.Height; y++)
                {
                    WorldMapTile wmt =
                        (WorldMapTile)building.Blueprint.Tiles.getWorldTileAt(x, y).Clone();

                    addWorldTileAt(wmt, null,
                        x + (int)building.Location.X, 
                        y + (int)building.Location.Y);
                }
            }
        }

        /// <summary>
        /// Get the world map character by it's ID assigned when it is added to the game world
        /// </summary>
        /// <param name="id">id of the characte</param>
        /// <returns>Character</returns>
        public WorldMapCharacter getWorldMapCharacterById(int id)
        {
            return worldMapCharacters.getCharacter(id);
        }

        public IEnumerable<WorldMapBuilding> getAllWorldMapBuildings()
        {
            foreach (KeyValuePair<int, WorldMapBuilding> wmb in worldMapBuildings.WorldMapBuildingsMap.GetEnumerator)
                yield return wmb.Value;
        }

        public IEnumerable<WorldMapCharacter> getAllWorldMapCharacters()
        {
            foreach (KeyValuePair<int, WorldMapCharacter> wmc in worldMapCharacters.CharacterMap.GetEnumerator)
                yield return wmc.Value;
        }

        public void update(float elapsed)
        {
            foreach (KeyValuePair<int, WorldMapCharacter> wmc in worldMapCharacters.CharacterMap.GetEnumerator)
                wmc.Value.updateMove(this);

            for (int i = 0; i < lstTileChangeTimers.Count; i++)
            {
                var tct = lstTileChangeTimers[i];

                tct.update(elapsed);

                if (tct.WorldMapTile != getWorldTileAt(tct.WorldMapTile.X, tct.WorldMapTile.Y))
                {
                    lstTileChangeTimers.RemoveAt(i);
                }
                else
                {
                    if (tct.ReadyToChange)
                    {
                        addWorldTileAt((WorldMapTile)tileFactory.create(tct.TileToChangeTo),
                            null,
                            tct.WorldMapTile.X,
                            tct.WorldMapTile.Y);

                        lstTileChangeTimers.RemoveAt(i);
                    }
                }
            }
        }

        public void draw(SpriteBatch sb, Texture2D tileTex)
        {
            int left = (int)cameraLoc.X;
            int right = left + drawableBounds.Width;
            int top = (int)cameraLoc.Y;
            int bottom = top + drawableBounds.Height;

            if (right > worldMapTiles.Width) right = worldMapTiles.Width;
            if (bottom > worldMapTiles.Height) bottom = worldMapTiles.Height;


            for (int x = left; x < right; x++)
            {
                for (int y = top; y < bottom; y++)
                {
                    Vector2 loc = new Vector2(x - left, y - top) * TILE_SIZE;
                    var wmt = worldMapTiles.getWorldTileAt(x, y);
                    Texture2D tex = tileTextureMap[wmt.TileType];

                    if (tex == null)
                        tex = tileTextureMap[TileType.INVALID];

                    sb.Draw(tex, loc, wmt.Color);

                    if (wmt.Character != null && wmt.Character.Drawable)
                    {
                        sb.Draw(tileTex,
                            loc,
                            Color.HotPink);
                    }
                }
            }
        }

        public int getBuildingContainingCharacter(int characterID)
        {
            return worldMapBuildings.getBuildingContainingCharacter(characterID);
        }

        public void putCharacterInBuilding(int buildingID, int characterID)
        {
            worldMapCharacters.getCharacter(characterID).Drawable = false;
            worldMapBuildings.putCharacterInBuilding(buildingID, characterID);
        }

        public void takeCharacterOutOfBuilding(int buildingID, int characterID)
        {
            worldMapCharacters.getCharacter(characterID).Drawable = true;
            worldMapBuildings.takeCharacterOutOfBuilding(buildingID, characterID);
        }

        public bool isCharacterInBuilding(int characterID)
        {
            return worldMapBuildings.isCharacterInBuilding(characterID);
        }

        public bool doesBuildingHaveCharacter(int buildingID)
        {
            return worldMapBuildings.doesBuildingHaveCharacter(buildingID);
        }

        public List<int> getAllCharactersInBuilding(int buildingID)
        {
            var i = worldMapBuildings.getAllCharactersInBuilding(buildingID);
            if (i == null) return new List<int>();
            return i;
        }
    }
}
