﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using KingdomsGame.ViewFramework;
using KingdomsGame.Factories;

namespace KingdomsGame
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        WorldMap worldMap;
        Texture2D tileTex;
        Thread consoleThread;
        GameTimer timer;
        GameController gameController;
        GameView gameView;
        ConsoleInput console;
        ViewportManager viewportManager;

        public WorldMap WorldMap { get { return worldMap; } }
        public GameTimer GameTimer { get { return timer; } }

        public Game1()
        {
            WorldMapGenerator worldMapGenerator = new WorldMapGenerator();
            WorldMapBuildingBlueprintFactory wmbf = new WorldMapBuildingBlueprintFactory();
            CivilizationPlacer civPlacer;
            worldMap = worldMapGenerator.generateMap(100, 50);
            TileConversionMap.init();
            gameController = new GameController(this);
            console = new ConsoleInput(gameController);
            graphics = new GraphicsDeviceManager(this);
            viewportManager = new ViewportManager(graphics);
            gameView = new GameView(gameController, viewportManager);
            consoleThread = new Thread(new ThreadStart(console.pollInput));
            timer = new GameTimer();
            Content.RootDirectory = "Content";

            this.IsMouseVisible = true;
            viewportManager.updateScreenResoltuion(1024, 576, true, false);

            civPlacer = new CivilizationPlacer(gameController);
            gameController.PlayerCivilization = civPlacer.generateCiv();

            consoleThread.Start();
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            TileTextureFactory ttf = new TileTextureFactory();
            EntityMap<TileType, Texture2D> tileTexMap = new EntityMap<TileType, Texture2D>();

            foreach(KeyValuePair<object, ICloneable> kvp in ttf.Dictionary)
            {
                tileTexMap.insert((TileType)kvp.Key,
                    Content.Load<Texture2D>((string)kvp.Value));
            }


            worldMap.TileTextureMap = tileTexMap;

            spriteBatch = new SpriteBatch(GraphicsDevice);
            tileTex = Content.Load<Texture2D>("tile");
            gameView.loadContent(Content);
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            consoleThread.Abort();
            base.OnExiting(sender, args);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            timer.update(elapsed);

            if (timer.ticked())
            {
                WorldMap.update(elapsed);
                gameController.PlayerCivilization.doTurn();

                foreach (WorldMapCharacter wmc in WorldMap.getAllWorldMapCharacters())
                {
                    if (wmc.Script != "")
                        gameController.ScriptRunner.loadAndExecuteScript(wmc.Script,
                            "charID",
                            wmc.Id.ToString());
                }
            }

            gameView.update(elapsed);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred,
                null,
                null,
                null,
                null,
                null,
                viewportManager.ViewportTransform);
            GraphicsDevice.Clear(Color.CornflowerBlue);
            worldMap.draw(spriteBatch, tileTex);
            gameView.draw(spriteBatch);
            base.Draw(gameTime);
            spriteBatch.End();
        }
    }
}
