﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame.WorldMapBuildingAttributes
{
    public abstract class BuildingAttribute : ICloneable
    {
        public abstract object Clone();
    }
}
