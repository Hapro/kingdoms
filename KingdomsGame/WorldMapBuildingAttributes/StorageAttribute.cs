﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoonSharp.Interpreter;

namespace KingdomsGame.WorldMapBuildingAttributes
{
    [MoonSharpUserData]
    public class StorageAttribute : BuildingAttribute
    {
        private EntityMap<Resource, int> resourcesMap;

        public StorageAttribute()
        {
            resourcesMap = new EntityMap<Resource, int>();
        }

        public int resourceCount(Resource res)
        {
            return resourcesMap[res];
        }

        public void addResource(Resource res)
        {
            if (resourcesMap.exists(res))
                resourcesMap.insert(res, resourcesMap.get(res) + 1);
            else
                resourcesMap.insert(res, 1);
        }

        public bool containsResource(Resource res)
        {
            return resourcesMap.exists(res) && resourcesMap.get(res) > 0;
        }

        public Resource removeResource(Resource res)
        {
            if (resourcesMap.exists(res))
            {
                int v = resourcesMap.get(res);
                if (v > 0)
                {
                    v -= 1;
                    resourcesMap.insert(res, v--);
                    return res;
                }
            }

            return Resource.NOTHING;
        }

        public override object Clone()
        {
            return new StorageAttribute();
        }

        public IEnumerable<KeyValuePair<Resource, int> > getAllResources()
        {
            foreach (KeyValuePair<Resource, int> res in resourcesMap.GetEnumerator)
                yield return res;
        }
    }
}
