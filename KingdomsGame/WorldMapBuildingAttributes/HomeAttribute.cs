﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoonSharp.Interpreter;

namespace KingdomsGame.WorldMapBuildingAttributes
{
    [MoonSharpUserData]
    class HomeAttribute : BuildingAttribute
    {
        private int food;

        public int Food
        {
            get { return food; }
            set { food = value; }
        }

        public override object Clone()
        {
            return base.MemberwiseClone();
        }
    }
}
