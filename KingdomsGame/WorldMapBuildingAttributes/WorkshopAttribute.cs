﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KingdomsGame.WorldMapBuildingAttributes;

namespace KingdomsGame.WorldMapBuildingAttributes
{
    public class WorkshopAttribute : BuildingAttribute
    {
        private Stack<CharacterAction> characterWorkActions;
        private Stack<CharacterAction> characterDestroyActions;
        private Stack<BuildAction> characterBuildActions;

        public Stack<CharacterAction> WorkActions
        {
            get { return characterWorkActions; }
            set { WorkActions = value; }
        }

        public Stack<CharacterAction> DestroyActions
        {
            get { return characterDestroyActions; }
            set { DestroyActions = value; }
        }

        public Stack<BuildAction> BuildActions
        {
            get { return characterBuildActions; }
            set { characterBuildActions = value; }
        }

        public WorkshopAttribute()
        {
            this.characterWorkActions = new Stack<CharacterAction>();
            this.characterDestroyActions = new Stack<CharacterAction>();
            this.characterBuildActions = new Stack<BuildAction>();
        }

        public override object Clone()
        {
            return new WorkshopAttribute();
        }
    }
}
