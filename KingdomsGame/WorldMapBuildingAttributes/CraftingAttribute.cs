﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame.WorldMapBuildingAttributes
{
    public class CraftingAttribute : BuildingAttribute
    {
        private Stack<CraftAction> characterCraftActions;
        private ResourceCraftingMap availableItemsToCraft;

        public Stack<CraftAction> CraftActions
        {
            get { return characterCraftActions; }
        }

        public CraftingAttribute(ResourceCraftingMap availableItemsToCraft)
        {
            this.characterCraftActions = new Stack<CraftAction>();
            this.availableItemsToCraft = availableItemsToCraft;
        }

        public IEnumerable<CraftingItem> getItemsThatCanBeCrafted()
        {
            return availableItemsToCraft.getAllCraftingItems();
        }

        public bool canCraftItem(Resource resource, StorageAttribute storage)
        {
            CraftingItem ci = availableItemsToCraft.craftingItemFor(resource);
            
            foreach(KeyValuePair<Resource, int> kvp in ci.ResourcesNeeded)
            {
                if (storage.resourceCount(kvp.Key) < kvp.Value)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Alters storage attribute to craft item, returning the item that was desired
        /// </summary>
        /// <param name="resource"></param>
        /// <param name="storage"></param>
        /// <returns></returns>
        public void craftItem(Resource resource, StorageAttribute storage)
        {
            CraftingItem ci = availableItemsToCraft.craftingItemFor(resource);

            foreach (KeyValuePair<Resource, int> kvp in ci.ResourcesNeeded)
            {
                for (int i = 0; i < kvp.Value; i++)
                    storage.removeResource(kvp.Key);
            }

            storage.addResource(resource);
        }

        public override object Clone()
        {
            return new CraftingAttribute(availableItemsToCraft);
        }
    }
}
