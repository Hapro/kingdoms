﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using KingdomsGame.Tables;

namespace KingdomsGame
{
    public class WorldMapCharacter : ICloneable
    {
        private int id;
        private WorldMapTile parentTile;
        private List<Vector2> lstPathToFollow;
        private string state;
        private EntityMap<object, object> data; //Arbitrary data assigned to this one
        private string script;
        private bool drawable;
        private Resource heldResource;

        public int Id { get { return id; } set { id = value; } }
        public WorldMapTile ParentTile { get { return parentTile; } set { parentTile = value; } }
        public List<Vector2> PathToFollow { set { lstPathToFollow = value; } }
        public bool IsFollowingPath 
        {
            get 
            { 
                return lstPathToFollow.Count > 0;
            }
        }
        public string State { get { return state; } set { state = value; } }
        public string Script { get { return script; } set { script = value; } }
        public bool IsIdle { get { return state == "idle"; } }
        public bool Drawable { get { return drawable; } set { drawable = value; } }
        public EntityMap<object, object> Data { get { return data; } }
        public Resource HeldResource { get { return heldResource; } set { heldResource = value; } }
        public bool AtEndOfPath
        {
            get { return !IsFollowingPath; }
        }

        public WorldMapCharacter(int id)
        {
            this.id = id;
            this.lstPathToFollow = new List<Vector2>();
            state = "";
            script = "";
            data = new EntityMap<object, object>();
            drawable = true;
            idle();
        }

        public virtual void stopFollowingPath()
        {
            lstPathToFollow.Clear();
        }

        public void updateMove(WorldMap wm)
        {
            if (IsFollowingPath)
            {
                Vector2 tileLocToMoveTo = popNextPathLocation();
                WorldMapTile tileToMoveTo = wm.getWorldTileAt(
                    (int)tileLocToMoveTo.X,
                    (int)tileLocToMoveTo.Y);

                if(tileToMoveTo.Passable || !parentTile.Passable)
                {
                    wm.putCharacterOnTile(this, tileToMoveTo);
                }
                else
                {
                    //keep searching for new path
                    //until one is found
                    List<Vector2> newPath = wm.findPath((int)
                        parentTile.X,
                       (int)parentTile.Y,
                       (int)lstPathToFollow[0].X,
                       (int)lstPathToFollow[0].Y);

                    if (newPath.Count > 0)
                        lstPathToFollow = newPath;
                }
            }
        }

        /// <summary>
        /// Returns the next location this character must move along the path it
        /// is following
        /// </summary>
        /// <returns></returns>
        public virtual Vector2 popNextPathLocation()
        {
            if (IsFollowingPath)
            {
                Vector2 pathItem = lstPathToFollow[lstPathToFollow.Count - 1];
                lstPathToFollow.RemoveAt(lstPathToFollow.Count - 1);
                return pathItem;
            }

            //TODO: Return null value
            return new Vector2(0, 0);
        }

        public void clearData()
        {
            this.data.clear();
        }

        public virtual void idle()
        {
            data.clear();
            this.state = "idle";
            this.script = "";
        }

        public Resource takeResource()
        {
            Resource res = heldResource;
            heldResource = Resource.NOTHING;
            return res;
        }

        public virtual void update()
        {

        }

        public virtual object Clone()
        {
            return new WorldMapCharacter(-1);
        }
    }
    public class WorldMapHuman : WorldMapCharacter
    {
        private WorldMapBuilding homeBuilding;
        private WorldMapBuilding workBuilding;
        private int hunger;

        public WorldMapBuilding HomeBuilding
        {
            get { return homeBuilding; }
            set { homeBuilding = value; }
        }

        public WorldMapBuilding WorkBuilding
        {
            get { return workBuilding; }
            set { workBuilding = value; }
        }

        public int Hunger
        {
            get { return hunger; }
            set { hunger = value; }
        }

        public WorldMapHuman()
            :base(0)
        {
            this.hunger = 10;
        }

        public override object Clone()
        {
            WorldMapHuman wmc = new WorldMapHuman();
            return wmc;
        }
    }
}
