﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    /// <summary>
    /// Some tiles change over time - maintain a list of these
    /// to maintain that change
    /// </summary>
    public class TileChangeTimer
    {
        private GameTimer timer;
        private int turnsToChange;
        private TileType tileToChangeTo;
        private WorldMapTile wmt;

        public WorldMapTile WorldMapTile
        {
            get { return wmt; }
            set { wmt = value; }
        }

        public TileType TileToChangeTo
        {
            get { return tileToChangeTo; }
        }

        public bool ReadyToChange
        {
            get { return turnsToChange == 0; }
        }

        public TileChangeTimer(int turnsToChange, TileType tileToChangeTo)
        {
            timer = new GameTimer();
            this.tileToChangeTo = tileToChangeTo;
            this.turnsToChange = turnsToChange;
        }

        public void update(float elapsed)
        {
            timer.update(elapsed);

            if (timer.ticked()) turnsToChange--;
        }
    }
}
