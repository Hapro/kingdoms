﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    public class CharacterAction
    {
        private bool finished;
        private WorldMapTile worldMapTile;
        private WorldMapBuilding worldMapBuilding;

        public WorldMapTile WorldMapTile
        {
            get { return worldMapTile; }
        }

        public WorldMapBuilding WorldMapBuilding
        {
            get { return worldMapBuilding; }
        }

        public bool Finished
        {
            get { return finished; }
            set { finished = value; }
        }

        public CharacterAction(WorldMapBuilding wmb, WorldMapTile wmt)
        {
            this.worldMapBuilding = wmb;
            this.worldMapTile = wmt;
            this.finished = false;
        }
    }

    public class CraftAction :CharacterAction
    {
        private Resource resourceToCraft;

        public Resource ResourceToCraft
        {
            get { return resourceToCraft; }
            set { resourceToCraft = value; }
        }

        public CraftAction(Resource resourceToCraft)
            : base(null, null)
        {
            this.resourceToCraft = resourceToCraft;
        }
    }

    public class BuildAction : CharacterAction
    {
        private Resource resourceToUse;

        public Resource ResourceToUse
        {
            get { return resourceToUse; }
            set { resourceToUse = value; }
        }

        public BuildAction(WorldMapBuilding wmb, WorldMapTile wmt, Resource resourceToUse)
            : base(wmb, wmt)
        {
            this.resourceToUse = resourceToUse;
        }
    }
}
