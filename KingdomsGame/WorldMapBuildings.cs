﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    public class WorldMapBuildings
    {
        private EntityMap<int, WorldMapBuilding> worldMapBuildingsMap;
        private EntityMap<int, int> charsInBuilding;
        private EntityMap<int, List<int>> buildingsWithChars;

        public EntityMap<int, WorldMapBuilding> WorldMapBuildingsMap
        {
            get { return worldMapBuildingsMap; }
        }

        public WorldMapBuildings()
        {
            charsInBuilding = new EntityMap<int, int>();
            buildingsWithChars = new EntityMap<int, List<int>>();
            worldMapBuildingsMap = new EntityMap<int, WorldMapBuilding>();

        }

        public WorldMapBuilding getBuilding(int id)
        {
            return worldMapBuildingsMap.get(id);
        }

        public int addBuilding(WorldMapBuilding building)
        {
            int id = worldMapBuildingsMap.Size + 1;
            worldMapBuildingsMap.insert(id, building);
            building.ID = id;
            return id;
        }

        public void removeBuilding(int id)
        {
            worldMapBuildingsMap.remove(id);
        }

        public int getBuildingContainingCharacter(int characterID)
        {
            return charsInBuilding.get(characterID);
        }

        public void putCharacterInBuilding(int buildingID, int characterID)
        {
            charsInBuilding.insert(characterID, buildingID);

            if (!buildingsWithChars.exists(buildingID))
                buildingsWithChars.insert(buildingID, new List<int>());

            buildingsWithChars.get(buildingID).Add(characterID);
        }

        public void takeCharacterOutOfBuilding(int buildingID, int characterID)
        {
            buildingsWithChars.get(buildingID).Remove(characterID);
            charsInBuilding.remove(characterID);
        }

        public bool isCharacterInBuilding(int characterID)
        {
            return charsInBuilding.exists(characterID);
        }

        public bool doesBuildingHaveCharacter(int buildingID)
        {
            return buildingsWithChars.exists(buildingID)
                && buildingsWithChars.get(buildingID).Count > 0;
        }

        public List<int> getAllCharactersInBuilding(int buildingID)
        {
            return buildingsWithChars.get(buildingID);
        }
    }
}
