﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    public class CraftingItem
    {
        private List<KeyValuePair<Resource, int>> resourcesNeeded;
        private string name;
        private Resource itemResource;

        public List<KeyValuePair<Resource, int>> ResourcesNeeded
        {
            get { return resourcesNeeded; }
        }

        public Resource ItemResource
        {
            get { return itemResource; }
        }

        public string Name
        {
            get { return name; }
        }

        public CraftingItem(Resource itemResource, List<KeyValuePair<Resource, int>> resourcesNeeded,
            string name)
        {
            this.itemResource = itemResource;
            this.resourcesNeeded = resourcesNeeded;
            this.name = name;
        }
    }

    public class ResourceCraftingMap
    {
        EntityMap<Resource, CraftingItem> craftingItems;

        public ResourceCraftingMap()
        {
            craftingItems = new EntityMap<Resource, CraftingItem>();
        }

        public void init()
        {
            craftingItems = new EntityMap<Resource, CraftingItem>();
            List<KeyValuePair<Resource, int>> r = new List<KeyValuePair<Resource, int>>();
        }

        public CraftingItem craftingItemFor(Resource res)
        {
            return craftingItems[res];
        }

        public IEnumerable<CraftingItem> getAllCraftingItems()
        {
            foreach (KeyValuePair<Resource, CraftingItem> kvp in craftingItems.GetEnumerator)
                yield return kvp.Value;
        }

        public void addCraftingItem(Resource res, CraftingItem ci)
        {
            craftingItems.insert(res, ci);
        }
    }
}
