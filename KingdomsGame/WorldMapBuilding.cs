﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using KingdomsGame.WorldMapBuildingAttributes;

namespace KingdomsGame
{
    /// <summary>
    /// Buildings consist of a collection of tiles in the world,
    /// with shape and type defined by the blueprint 
    /// </summary>
    public class WorldMapBuilding : ICloneable
    {
        private Vector2 location; //Top-left location of building
        private WorldMapBuildingBlueprint blueprint;
        private int id;
        private EntityMap<BuildingAttributes, BuildingAttribute> buildingAttributesMap;
        private string name;

        public int EntranceX { get { return (int)EntranceTileLocation.X; } }
        public int EntranceY { get { return (int)EntranceTileLocation.Y; } }
        public Vector2 EntranceTileLocation { get { return new Vector2(location.X - 1, location.Y); } }
        public Vector2 Location{ get { return location; } set{ location = value; } }
        public WorldMapBuildingBlueprint Blueprint { get { return blueprint; } }
        public int ID { get { return id; } set { id = value; } }
        public string Name { get { return name; } }
        public EntityMap<BuildingAttributes, BuildingAttribute> BuildingAttributesList
        {
            get { return buildingAttributesMap; }
        }

        public Rectangle Boundary 
        {
            get
            {
                return new Rectangle((int)location.X,
                    (int)location.Y,
                    blueprint.BuildingBoundary.Width,
                    blueprint.BuildingBoundary.Height);
            }
        }

        public WorldMapBuilding(WorldMapBuildingBlueprint blueprint, string name)
        {
            this.name = name;
            buildingAttributesMap = new EntityMap<BuildingAttributes, BuildingAttribute>();
            this.blueprint = blueprint;
            id = -1;
        }

        public bool checkBuildingIntegrity(WorldMap worldMap)
        {
            return blueprint.checkBuildingIntegrity(this, worldMap);
        }

        public bool hasAttribute(BuildingAttributes ba)
        {
            return BuildingAttributesList.exists(ba);
        }

        public StorageAttribute getStorageAttribute()
        {
            return (StorageAttribute)BuildingAttributesList[BuildingAttributes.STORAGE];
        }

        public WorkshopAttribute getWorkshopAttribute()
        {
            return (WorkshopAttribute)BuildingAttributesList[BuildingAttributes.WORKSHOP];
        }

        public CraftingAttribute getCraftingAttribute()
        {
            return (CraftingAttribute)BuildingAttributesList[BuildingAttributes.CRAFTING];
        }

        public virtual object Clone()
        {
            WorldMapBuilding wmb = new WorldMapBuilding((WorldMapBuildingBlueprint)this.blueprint.Clone(),
                name);
            wmb.buildingAttributesMap = new EntityMap<BuildingAttributes, BuildingAttribute>();

            foreach(KeyValuePair<BuildingAttributes, BuildingAttribute> kvp in 
                this.buildingAttributesMap.GetEnumerator)
            {
                wmb.BuildingAttributesList.insert(kvp.Key, 
                    (BuildingAttribute)kvp.Value.Clone());
            }

            return wmb;
        }

        public override string ToString()
        {
            string str = "ID: " + this.id.ToString() + '\n';

            if (getStorageAttribute() != null)
            {
                str += "Storeroom: \n";
                foreach (KeyValuePair<Resource, int> kvp in getStorageAttribute().getAllResources())
                {
                    str += kvp.Key + ", " + kvp.Value.ToString() + "\n";
                }
            }

            if (getWorkshopAttribute() != null)
                str += "Workshop\n";

            if(getCraftingAttribute() != null)
            {
                str += "Crafting station\n";
            }

            return str;
        }
    }
}
