﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
    public class GameTimer
    {
        private float interval = 100f;
        private float counter = 0.0f;

        public float Interval { get { return interval; } set { interval = value; } }

        public void update(float elapsed)
        {
            if (counter >= interval)
                counter = 0.0f;

            counter += elapsed;
        }

        public bool ticked()
        {
            return counter >= interval;
        }
    }
}
