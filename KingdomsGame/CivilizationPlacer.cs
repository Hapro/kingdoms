﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KingdomsGame.Factories;

namespace KingdomsGame
{
    /// <summary>
    /// Class which places a civilization on the world map,
    /// and gives them starting resources
    /// </summary>
    public class CivilizationPlacer
    {
        private GameController gameController;

        public CivilizationPlacer(GameController gameController)
        {
            this.gameController = gameController;
        }

        private void placeStartBuildingsAt(PlayerCivilization civ, int x, int y)
        {
            //Create buildings
            WorldMapBuildingFactory wmbf = new WorldMapBuildingFactory();
            WorldMapBuilding workshop = (WorldMapBuilding)wmbf.create("Wood workshop");
            WorldMapBuilding storage = (WorldMapBuilding)wmbf.create("Wood storage hut");
            WorldMapBuilding home = (WorldMapBuilding)wmbf.create("Wood home");

            civ.AvailableBuildings.Add((WorldMapBuilding)wmbf.create("Wood workshop"));
            civ.AvailableBuildings.Add((WorldMapBuilding)wmbf.create("Wood wall"));
            civ.AvailableBuildings.Add((WorldMapBuilding)wmbf.create("Wood home"));
            civ.AvailableBuildings.Add((WorldMapBuilding)wmbf.create("Wheat seeds"));

            for (int i = 0; i < 5; i++)
                workshop.getStorageAttribute().addResource(Resource.WHEAT);

            //add builgins to world map. register to player civ
            civ.addBuilding(gameController.WorldMap.addBuildingToWorldMap(workshop, x, y));
            civ.addBuilding(gameController.WorldMap.addBuildingToWorldMap(storage, x + 3, y + 1));
            civ.addBuilding(gameController.WorldMap.addBuildingToWorldMap(home, x, y - 10));
            gameController.WorldMap.addBuildingToWorldMap(home, x, y - 10);


            gameController.WorldMap.fillInBuilding(workshop.ID);
            gameController.WorldMap.fillInBuilding(storage.ID);
            gameController.WorldMap.fillInBuilding(home.ID);

            int charID = gameController.addCharacterToGame("HUMAN", x - 1, y);
            civ.addCharacter(charID);
            gameController.WorldMap.putCharacterInBuilding(workshop.ID, charID);

            //set this guys home and work
            WorldMapHuman human = (WorldMapHuman)gameController.WorldMap.getWorldMapCharacterById(charID);
            human.WorkBuilding = workshop;
            human.HomeBuilding = home;

        }

        public PlayerCivilization generateCiv()
        {
            PlayerCivilization civ = new PlayerCivilization(gameController);
            WorldMap wm = gameController.WorldMap;

            placeStartBuildingsAt(civ, 20, 20);
            return civ;
        }
    }
}
