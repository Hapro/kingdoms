﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoonSharp.Interpreter;
using KingdomsGame.WorldMapBuildingAttributes;

namespace KingdomsGame.ScriptFacades
{
    [MoonSharpUserData]
    public class WorldMapBuildingFacade
    {
        private WorldMapBuilding worldMapBuilding;

        public WorldMapBuildingFacade(WorldMapBuilding worldMapBuilding)
        {
            this.worldMapBuilding = worldMapBuilding;
        }

        public bool checkIntegrity(WorldMap wm)
        {
            return worldMapBuilding.checkBuildingIntegrity(wm);
        }

        public int entranceX()
        {
            return (int)worldMapBuilding.EntranceTileLocation.X;
        }

        public int entranceY()
        {
            return (int)worldMapBuilding.EntranceTileLocation.Y;
        }

        public bool hasStorageAttribute()
        {
            return worldMapBuilding.getStorageAttribute() != null;
        }

        public bool hasWorkshopAttribute()
        {
            return worldMapBuilding.getWorkshopAttribute() != null;
        }

        public StorageAttribute getStorageAttribute()
        {
            return worldMapBuilding.getStorageAttribute();
        }

        public WorkshopAttribute getWorkshopAttribute()
        {
            return worldMapBuilding.getWorkshopAttribute();
        }

        public string listResources()
        {
            return worldMapBuilding.getStorageAttribute().ToString();
        }

    }
}
