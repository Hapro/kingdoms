﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoonSharp.Interpreter;

namespace KingdomsGame.ScriptFacades
{
    /// <summary>
    /// Provides a hook for the script to affect world map characters 
    /// in a safe way
    /// </summary>
    [MoonSharpUserData]
    public class WorldMapCharacterFacade
    {
        private WorldMapCharacter worldMapCharacter;
        public EntityMap<object, object> Data { get { return worldMapCharacter.Data; } }

        public WorldMapCharacterFacade(WorldMapCharacter wmc)
        {
            worldMapCharacter = wmc;
        }

        /// <summary>
        /// Returns true if this character is on the specified tile
        /// </summary>
        /// <param name="x">x pos</param>
        /// <param name="y">y pos</param>
        /// <returns>If the character is on the tile</returns>
        public bool isOnTile(int x, int y)
        {
            return worldMapCharacter.ParentTile.X == x &&
                worldMapCharacter.ParentTile.Y == y;
        }

        public void insertData(object key, object value)
        {
            Data[key] = value;
        }

        public bool hasPath(){ return worldMapCharacter.IsFollowingPath; }

        public int X() { return worldMapCharacter.ParentTile.X; }

        public int Y() { return worldMapCharacter.ParentTile.Y; }

        public int ID() {return worldMapCharacter.Id; }

        public string getState(){ return worldMapCharacter.State; }

        public void setState(string state){ worldMapCharacter.State = state; }

        public void idle(){ worldMapCharacter.idle();}

        public void setScript(string scriptName){ worldMapCharacter.Script = scriptName; }

        private void changeScript()
        {
            worldMapCharacter.idle();
        }

        public Resource takeResource()
        {
            Resource res = worldMapCharacter.HeldResource;
            worldMapCharacter.HeldResource = Resource.NOTHING;
            return res;
        }

        public void giveResource(Resource res)
        {
            worldMapCharacter.HeldResource = res;
        }

    }
}
