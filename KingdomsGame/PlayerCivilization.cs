﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KingdomsGame.Tables;
using KingdomsGame.Factories;
using MoonSharp.Interpreter;

namespace KingdomsGame
{
    [MoonSharpUserData]
    public class PlayerCivilization
    {
        private GameController gameController;
        private List<int> lstCharacterIDs;
        private List<int> lstBuildingIDs;
        private EntityMap<int, List<int>> buildingRelationsMap;
        private List<WorldMapBuilding> lstAvailableBuildings;

        public List<int> BuildingIDs { get { return lstBuildingIDs; } }

        public List<int> CharacterIDs
        {
            get
            { 
                return lstCharacterIDs;
            }
        }

        public List<WorldMapBuilding> AvailableBuildings
        {
            get { return lstAvailableBuildings; }
        }

        public PlayerCivilization(GameController gameController)
        {
            buildingRelationsMap = new EntityMap<int, List<int>>();
            lstCharacterIDs = new List<int>();
            lstBuildingIDs = new List<int>();
            this.gameController = gameController;
            lstAvailableBuildings = new List<WorldMapBuilding>();
        }

        public void addCharacter(int id)
        {
            lstCharacterIDs.Add(id);
        }
        public void addBuilding(int id)
        {
            this.lstBuildingIDs.Add(id);
        }

        /// <summary>
        /// Adds a one-way relation from the sender to the reciever
        /// </summary>
        /// <param name="senderID">ID of building</param>
        /// <param name="recieverID">ID of building</param>
        public void addBuildingRelation(int senderID, int recieverID)
        {
            if(!buildingRelationsMap.exists(senderID))
                buildingRelationsMap.insert(senderID, new List<int>());

            if (!buildingRelationsMap.get(senderID).Contains(recieverID))
                buildingRelationsMap.get(senderID).Add(recieverID);
        }

        /// <summary>
        /// Removes one-way relation between buildings
        /// </summary>
        /// <param name="senderID">ID of building</param>
        /// <param name="recieverID">ID of building</param>
        public void removeBuildingRelation(int senderID, int recieverID)
        {
            if (buildingRelationsMap.exists(senderID))
            {
                if (buildingRelationsMap.get(senderID).Contains(recieverID))
                    buildingRelationsMap.get(senderID).Remove(recieverID);
            }
        }

        public int getNoRelatedBuildings(int id)
        {
            return getRelatedBuildings(id).Count();
        }

        public bool hasRelatedBuilding(int id, BuildingAttributes att)
        {
            if(buildingRelationsMap.exists(id))
            {
                foreach (int i in buildingRelationsMap.get(id))
                {
                    if (gameController.WorldMap.getBuildingById(i).BuildingAttributesList.exists(att))
                        return true;
                }
            }

         return false;
        }

        public IEnumerable<int> getRelatedBuildings(int id)
        {
            IEnumerable<int> l = buildingRelationsMap.get(id);
            if (l != null)
                return l;
            else
                return new List<int>();
        }

        public IEnumerable<int> getRelatedBuildings(int id, BuildingAttributes att)
        {
            if (buildingRelationsMap.exists(id))
            {
                foreach (int i in buildingRelationsMap.get(id))
                {
                    if (gameController.WorldMap.getBuildingById(i).BuildingAttributesList.exists(att))
                        yield return i;
                }
            }
        }

        public Stack<WorldMapCharacter> getCharactersBy(Func<WorldMapCharacter, bool> query)
        {
            Stack<WorldMapCharacter> lst = new Stack<WorldMapCharacter>();

            for (int i = 0; i < lstCharacterIDs.Count; i++)
            {
                WorldMapCharacter wmc = gameController.WorldMap.
                    getWorldMapCharacterById(lstCharacterIDs[i]);

                if (query(wmc))
                    lst.Push(wmc);
            }
            return lst;
        }

        public void addDestroyTileAction(WorldMapBuilding building, WorldMapTile tile)
        {
            building.getWorkshopAttribute().DestroyActions.Push(new CharacterAction(building, tile));
        }
        
        public void addWorkTileAction(WorldMapBuilding building, WorldMapTile tile)
        {
            building.getWorkshopAttribute().WorkActions.Push(new CharacterAction(building,
                tile));
        }

        public void addBuildTileAction(WorldMapBuilding building, WorldMapTile tile,
            TileType tileToBuild)
        {
            Resource resourceNeeded = TileConversionMap.getResourceNeededToBuildTile(tileToBuild);
            building.getWorkshopAttribute().BuildActions.Push(
                new BuildAction(building, tile, resourceNeeded));
        }

        public void addCraftResourceAction(WorldMapBuilding building, Resource resource)
        {
            building.getCraftingAttribute().CraftActions.Push(
                new CraftAction(resource));
        }

        public void assignCharactersToWorkActions(WorldMapBuilding wmb, Stack<CharacterAction> stack)
        {
            //go through all characters inside
            foreach (int wmcId in gameController.WorldMap.getAllCharactersInBuilding(wmb.ID))
            {
                var wmc = gameController.WorldMap.getWorldMapCharacterById(wmcId);

                //if the character is free
                if (wmc.IsIdle && stack.Count > 0)
                {
                    wmc.Data["CharacterAction"] = stack.Pop();
                    wmc.State = "ready";
                    wmc.Script = "gatherResources";
                }
            }
        }

        public void assignCharactersToDestroyActions(WorldMapBuilding wmb, Stack<CharacterAction> stack)
        {
            //go through all characters inside
            foreach (int wmcId in gameController.WorldMap.getAllCharactersInBuilding(wmb.ID))
            {
                var wmc = gameController.WorldMap.getWorldMapCharacterById(wmcId);

                //if the character is free
                if (wmc.IsIdle && stack.Count > 0)
                {
                    WorldMapTile adjacentTile = null;

                    foreach (WorldMapTile wmt in gameController.WorldMap.getCrossAdjacentTiles(
                        stack.Peek().WorldMapTile.X, stack.Peek().WorldMapTile.Y))
                    {
                        if (wmt.Passable)
                            adjacentTile = wmt;
                    }

                    //Only assign table item if there exists a clear adjacent tile
                    if (adjacentTile != null)
                    {
                        wmc.Data["AdjacentTile"] = adjacentTile;
                        wmc.Data["CharacterAction"] = stack.Pop();
                        wmc.State = "ready";
                        wmc.Script = "destroyTile";
                    }
                }
            }
        }

        public void assignCharactersToBuildActions(WorldMapBuilding wmb, Stack<BuildAction> stack)
        {
            //go through all characters inside
            foreach (int wmcId in gameController.WorldMap.getAllCharactersInBuilding(wmb.ID))
            {
                var wmc = gameController.WorldMap.getWorldMapCharacterById(wmcId);

                //if the character is free
                if (wmc.IsIdle && stack.Count > 0)
                {
                    wmc.Data["CharacterAction"] = stack.Pop();
                    wmc.State = "ready";
                    wmc.Script = "build";
                }
            }
        }

        public void doTurn()
        {
            foreach (int i in lstBuildingIDs)
            {
                var b = gameController.WorldMap.getBuildingById(i);

                if (b.getWorkshopAttribute() != null)
                {
                    if (b.getWorkshopAttribute().WorkActions.Count > 0)
                        assignCharactersToWorkActions(b, b.getWorkshopAttribute().WorkActions);

                    if (b.getWorkshopAttribute().BuildActions.Count > 0)
                        assignCharactersToBuildActions(b, b.getWorkshopAttribute().BuildActions);

                    if (b.getWorkshopAttribute().DestroyActions.Count > 0)
                        assignCharactersToDestroyActions(b, b.getWorkshopAttribute().DestroyActions);
                }

                if(b.getCraftingAttribute() != null)
                {
                    if(b.getCraftingAttribute().CraftActions.Count > 0)
                    {
                        var craftAction = b.getCraftingAttribute().CraftActions.Pop();

                        if (b.getCraftingAttribute().canCraftItem(craftAction.ResourceToCraft,
                            b.getStorageAttribute()))
                        {
                            b.getCraftingAttribute().craftItem(craftAction.ResourceToCraft, 
                                b.getStorageAttribute());
                        }
                    }
                }
            }
            updateCharacters();
        }

        private void updateCharacters()
        {
            foreach (int id in lstCharacterIDs)
            {
                WorldMapHuman wmc = 
                    (WorldMapHuman)gameController.WorldMap.getWorldMapCharacterById(id);
                //Tell character to go home and eat if hungry and not
                //doing anything
                if (wmc.Hunger == 0 && wmc.State == "idle")
                {
                    wmc.Script = "goHomeAndEat";
                    wmc.State = "ready";
                }
            }
        }
    }
}
