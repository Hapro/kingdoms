﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KingdomsGame
{
 /// <summary>
    /// Provides a 2-dimension grid of arbitrary objects
    /// </summary>
    public class Grid<T>
    {
        private List<object> lst;
        private int width;
        private int height;

        public int Width { get { return width; } }
        public int Height { get { return height; } }

        public Grid(int width, int height)
        {
            this.width = width;
            this.height = height;
            lst = new List<object>();
            clear();
        }

        public void clear()
        {
            lst.Clear();

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < width; y++)
                {
                    lst.Add(default(T));
                }
            }
        }

        /// <summary>
        /// Get the world tile at the given x and y co-ordinates
        /// </summary>
        /// <param name="x">x co-ordinate</param>
        /// <param name="y">y co-ordinate</param>
        /// <returns>World map tile at location. Returns null if no tile is found</returns>
        public T getItemAt(int x, int y)
        {
            if (!validLoc(x, y))
                return default(T);

            return (T)lst[hashFunction(x, y)];
        }

        public bool addItemAt(T item, int x, int y)
        {
            if (validLoc(x, y))
            {
                lst[hashFunction(x, y)] = item;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Convert x and y co-ordinates to an index to store an item
        /// in the grid
        /// </summary>
        /// <param name="x">x co-ordinate</param>
        /// <param name="y">y co-ordinate</param>
        /// <returns>index relating to x and y co-ordinate</returns>
        private int hashFunction(int x, int y)
        {
            return (x * width) + y;
        }

        private bool validLoc(int x, int y)
        {
            return (x < width && y < height && x >= 0 && y >= 0);
        }
    }
}
