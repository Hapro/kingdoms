﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace KingdomsGame
{
    /// <summary>
    /// Class to map tiles to tiles when being worked
    /// i.e. a wood tile is mapped to a grass tile when worked
    /// </summary>
    public static class TileConversionMap
    {
        private static EntityMap<TileType, TileType> tileToTileWhenWorkedMap;
        private static EntityMap<TileType, TileType> tileToTileWhenDestroyedMap;
        private static EntityMap<TileType, Resource> tileToResourceWhenWorkedMap;
        private static EntityMap<Resource, TileType> resourceToEquivalentTileMap;
        private static EntityMap<TileType, Resource> tileToEquivalnetResourceMap;

        public static void init()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"Content\Data\tileConversionData.xml");
            XmlNodeList node = xmlDoc.GetElementsByTagName("tileToTileWhenWorked");

            tileToTileWhenWorkedMap = new EntityMap<TileType, TileType>();
            tileToTileWhenDestroyedMap = new EntityMap<TileType, TileType>();
            tileToResourceWhenWorkedMap = new EntityMap<TileType, Resource>();
            resourceToEquivalentTileMap = new EntityMap<Resource, TileType>();
            tileToEquivalnetResourceMap = new EntityMap<TileType, Resource>();

            foreach (XmlNode n in node[0].ChildNodes)
            {
                TileType k = (TileType)Enum.Parse(typeof(TileType), n.Attributes[0].Value);
                TileType v = (TileType)Enum.Parse(typeof(TileType), n.Attributes[1].Value);
                tileToTileWhenWorkedMap.insert(k, v);
            }

            node = xmlDoc.GetElementsByTagName("tileToTileWhenDestroyed");

            foreach (XmlNode n in node[0].ChildNodes)
            {
                TileType k = (TileType)Enum.Parse(typeof(TileType), n.Attributes[0].Value);
                TileType v = (TileType)Enum.Parse(typeof(TileType), n.Attributes[1].Value);
                tileToTileWhenDestroyedMap.insert(k, v);
            }

            node = xmlDoc.GetElementsByTagName("tileToResourceWhenWorked");

            foreach (XmlNode n in node[0].ChildNodes)
            {
                TileType k = (TileType)Enum.Parse(typeof(TileType), n.Attributes[0].Value);
                Resource v = (Resource)Enum.Parse(typeof(Resource), n.Attributes[1].Value);
                tileToResourceWhenWorkedMap.insert(k, v);
            }

            node = xmlDoc.GetElementsByTagName("resourceToEquivalentTile");

            foreach (XmlNode n in node[0].ChildNodes)
            {
                TileType v = (TileType)Enum.Parse(typeof(TileType), n.Attributes[1].Value);
                Resource k = (Resource)Enum.Parse(typeof(Resource), n.Attributes[0].Value);
                resourceToEquivalentTileMap.insert(k, v);
                tileToEquivalnetResourceMap.insert(v, k);
            }
        }

        public static TileType getTileWhenDestroyed(TileType tileType)
        {
            if (tileToTileWhenDestroyedMap.exists(tileType))
                return tileToTileWhenDestroyedMap[tileType];

            return tileType;
        }

        /// <summary>
        /// Gets the tile that the supplied tile will turn into when worked
        /// </summary>
        /// <param name="tileType">tile to work</param>
        /// <returns></returns>
        public static TileType getTileWhenWorked(TileType tileType)
        {
            if (tileToTileWhenWorkedMap.exists(tileType))
                return tileToTileWhenWorkedMap[tileType];

            return tileType;
        }
        
        /// <summary>
        /// Get the resource which the associated tile produces when worked
        /// </summary>
        /// <param name="tileType">tile to get resource from</param>
        /// <returns></returns>
        public static Resource getResourceFromTile(TileType tileType)
        {
            if(tileToResourceWhenWorkedMap.exists(tileType))
                return tileToResourceWhenWorkedMap[tileType];

            return Resource.NOTHING;
        }

        /// <summary>
        /// Gets the tile which is built by the associated resource
        /// </summary>
        /// <param name="res"></param>
        /// <returns></returns>
        public static TileType getTileForResource(Resource res)
        {
            if (resourceToEquivalentTileMap.exists(res))
                return resourceToEquivalentTileMap[res];

            return TileType.INVALID;
        }

        /// <summary>
        /// Gets the resource necessary to build a tile of the supplied type
        /// </summary>
        /// <param name="tileType">tile we want to build</param>
        /// <returns></returns>
        public static Resource getResourceNeededToBuildTile(TileType tileType)
        {
            if (tileToEquivalnetResourceMap.exists(tileType))
                return tileToEquivalnetResourceMap[tileType];

            return Resource.NOTHING;
        }
    }
}
