﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KingdomsGame.Factories;

namespace KingdomsGame
{
    /// <summary>
    /// Provides 'atomic' functions which can be hooked into
    /// by scripts, the View, external classes etc
    /// 
    /// This is the main way the rest of the program will interface with the
    /// game 'model'. The game world persists by itself, and is manipulated by
    /// scripts, AI, the player, and other external agents via the game controller
    /// </summary>
    public class GameController
    {
        private Game1 game;
        private ScriptRunner scriptRunner;
        private WorldMapCharacterFactory worldMapCharacterFactory;
        private WorldMapBuildingFactory worldMapBuildingFactory;
        private PlayerCivilization playerCivilization;

        public ScriptRunner ScriptRunner { get { return scriptRunner; } }
        public WorldMap WorldMap { get { return game.WorldMap; } }
        public PlayerCivilization PlayerCivilization 
        {
            get { return playerCivilization; }
            set { playerCivilization = value; }
        }

        public GameController(Game1 game)
        {
            TileFactory tileFactory = new TileFactory();
            this.game = game;
            this.worldMapCharacterFactory = new WorldMapCharacterFactory();
            this.worldMapBuildingFactory = new WorldMapBuildingFactory();

            scriptRunner = new ScriptRunner(this);
        }
        public void setTimerInterval(float interval)
        {
            game.GameTimer.Interval = interval;
        }
        
        /// <summary>
        /// Sets the character moving along a path 
        /// </summary>
        /// <param name="id">id of character</param>
        /// <param name="xLoc">x location to move to</param>
        /// <param name="yLoc">y location to move to</param>
        public void moveCharacterTo(int id, int x, int y)
        {
            WorldMapCharacter wmc = WorldMap.getWorldMapCharacterById(id);
            if (WorldMap.isCharacterInBuilding(id))
                WorldMap.takeCharacterOutOfBuilding(WorldMap.getBuildingContainingCharacter(id), id);

            wmc.PathToFollow = WorldMap.findPath(
                wmc.ParentTile.X,
                wmc.ParentTile.Y,
                x,
                y);
        }

        /// <summary>
        /// Makes the selected character destroy the tile
        /// at x and y co-ords 
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void makeCharacterDestroyTile(int characterId, int x, int y)
        {
            WorldMapCharacter c = WorldMap.getWorldMapCharacterById(characterId);

            if (c != null)
            {
                WorldMapTile wmt = game.WorldMap.getWorldTileAt(x, y);
                TileType t = TileConversionMap.getTileWhenDestroyed(wmt.TileType);
                convertTile(x, y, t);
            }
        }

        /// <summary>
        /// Makes the specified character work the tile they are standing on
        /// </summary>
        /// <param name="characterId">Id assigned to character</param>
        public void makeCharacterWorkTile(int characterId)
        {
            WorldMapCharacter c = WorldMap.getWorldMapCharacterById(characterId);

            if (c != null)
            {
                c.HeldResource = TileConversionMap.getResourceFromTile(c.ParentTile.TileType);
                TileType t = TileConversionMap.getTileWhenWorked(c.ParentTile.TileType);
                convertTile(c.ParentTile.X, c.ParentTile.Y, t);
            }
        }

        public void makeCharacterBuildOnTile(int characterId)
        {
            WorldMapCharacter c = WorldMap.getWorldMapCharacterById(characterId);

            if (c != null)
            {
                TileType t = TileConversionMap.getTileForResource(c.takeResource());
                convertTile(c.ParentTile.X, c.ParentTile.Y, t);
            }
        }

        public void convertTile(int x, int y, string newTileType)
        {
            TileType t;
            bool b = Enum.TryParse<TileType>(newTileType, out t);

            if (b) convertTile(x, y, t);
        }

        public void convertTile(int x, int y, TileType newTileType)
        {
            var t = WorldMap.getWorldTileAt(x, y);
            if (t != null)
            {
                TileChangeTimer tct;
                TileFactory tileFactory = new TileFactory();
                WorldMapTile newTile = (WorldMapTile)tileFactory.create(newTileType, out tct);
                
                WorldMap.addWorldTileAt(newTile,tct, x, y);

                if (t.Character != null)
                {
                    WorldMap.putCharacterOnTile(t.Character, newTile);
                } 
            }
        }

        public int addCharacterToGame(string type, int x, int y)
        {
            int id = WorldMap.addCharacterToWorldMap(
                (WorldMapCharacter)worldMapCharacterFactory.
                create(type), x, y);
            return id;
        }

        public int addBuildingToGame(string type, int x, int y)
        {
            return addBuildingToGame(
                (WorldMapBuilding)worldMapBuildingFactory.
                create(type), x, y);
        }

        public int addBuildingToGame(WorldMapBuilding wmb, int x, int y)
        {
            int id = WorldMap.addBuildingToWorldMap(wmb, x, y);
            return id;
        }

        public bool checkBuildingIntegrity(int id)
        {
            return WorldMap.getBuildingById(id).checkBuildingIntegrity(WorldMap);
        }

        public void exitGame()
        {
            game.Exit();
        }
    }
}
