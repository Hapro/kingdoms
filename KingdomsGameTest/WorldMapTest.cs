﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KingdomsGame;
using KingdomsGame.Factories;

namespace KingdomsGameTest
{
    [TestClass]
    public class WorldMapTest
    {
        [TestMethod]
        public void testAddBuildingToWorldMap()
        {
            WorldMapBuildingFactory wmbf = new WorldMapBuildingFactory();
            WorldMap wm = new WorldMap(10, 10);
            WorldMapBuilding wmb = (WorldMapBuilding)wmbf.create("HUT");
            int id = wm.addBuildingToWorldMap(wmb, 0, 0);

            Assert.AreEqual(false, wm.getBuildingById(id).checkBuildingIntegrity(wm));
        }

        [TestMethod]
        public void testAddCharacterToGame()
        {
            WorldMap wm = new WorldMap(100, 100);
            int id1 = wm.addCharacterToWorldMap(5, 5);
            int id2 = wm.addCharacterToWorldMap(54, 23);
            int id3 = wm.addCharacterToWorldMap(1000, 100);

            Assert.IsNotNull(wm.getWorldMapCharacterById(id1));
            Assert.IsNotNull(wm.getWorldMapCharacterById(id2));
            Assert.IsNull(wm.getWorldMapCharacterById(id3));
            Assert.IsNull(wm.getWorldMapCharacterById(-2));
        }

        [TestMethod]
        public void testPutCharacterOnTile()
        {
            WorldMap wm = new WorldMap(100, 100);
            WorldMapCharacter wmc = new TestCharacter(0);

            wm.addCharacterToWorldMap(wmc, 0, 0);
            Assert.AreEqual(true, wm.doesTileHaveCharacter(wm.getWorldTileAt(0, 0)));
        }

        [TestMethod]
        public void testGetWorldMapTileAt()
        {
            WorldMap wm = new WorldMap(100, 100);
            wm.getWorldTileAt(5, 5).TileType = TileType.GRASS;

            Assert.AreEqual(TileType.GRASS, wm.getWorldTileAt(5, 5).TileType);
        }
    }
}
