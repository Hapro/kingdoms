﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KingdomsGame;
using KingdomsGame.Factories;

namespace KingdomsGameTest
{
    [TestClass]
    public class WorldMapBuildingTest
    {
        [TestMethod]
        public void testPutCharacterInBuilding()
        {
            WorldMapBuildingFactory wmbf = new WorldMapBuildingFactory();
            WorldMapBuildings wmbs = new WorldMapBuildings();
            int id = wmbs.addBuilding((WorldMapBuilding)wmbf.create("HUT"));
            WorldMapCharacter wmc = new WorldMapCharacter(0);

            Assert.AreEqual(false, wmbs.doesBuildingHaveCharacter(id));
            Assert.AreEqual(false, wmbs.isCharacterInBuilding(wmc.Id));

            wmbs.putCharacterInBuilding(id, wmc.Id);

            Assert.AreEqual(true, wmbs.doesBuildingHaveCharacter(id));
            Assert.AreEqual(wmc.Id, wmbs.getAllCharactersInBuilding(id)[0]);
            Assert.AreEqual(true, wmbs.isCharacterInBuilding(wmc.Id));

            wmbs.takeCharacterOutOfBuilding(id, wmc.Id);

            Assert.AreEqual(false, wmbs.doesBuildingHaveCharacter(id));
            Assert.AreEqual(false, wmbs.isCharacterInBuilding(wmc.Id));

        }

        [TestMethod]
        public void testBuildingIntegrity()
        {
            WorldMapBuildingFactory wmbf = new WorldMapBuildingFactory();
            WorldMap wm = new WorldMap(10, 10);
            //Default blueprint - this test will fail after changes are made
            WorldMapBuilding wmb = (WorldMapBuilding)wmbf.create("HUT");
            Assert.AreEqual(false, wmb.checkBuildingIntegrity(wm));

            wmb.Location = new Microsoft.Xna.Framework.Vector2(0, 0);

            wm.getWorldTileAt(0, 0).TileType = TileType.WOOD;
            wm.getWorldTileAt(1, 0).TileType = TileType.WOOD;
            wm.getWorldTileAt(0, 1).TileType = TileType.WOOD;
            wm.getWorldTileAt(1, 1).TileType = TileType.WOOD;

            Assert.AreEqual(true, wmb.checkBuildingIntegrity(wm));

            wmb.Location = new Microsoft.Xna.Framework.Vector2(1, 1);

            Assert.AreEqual(false, wmb.checkBuildingIntegrity(wm));

            wm.getWorldTileAt(1, 1).TileType = TileType.WOOD;
            wm.getWorldTileAt(1, 2).TileType = TileType.WOOD;
            wm.getWorldTileAt(2, 1).TileType = TileType.WOOD;
            wm.getWorldTileAt(2, 2).TileType = TileType.WOOD;

            Assert.AreEqual(true, wmb.checkBuildingIntegrity(wm));

        }
    }
}
