﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KingdomsGame;

namespace KingdomsGameTest
{
    public class TestCharacter : WorldMapCharacter
    {
        public TestCharacter(int id)
            :base(id)
        {
            
        }
    }

    [TestClass]
    public class WorldMapCharactersTest
    {
        [TestMethod]
        public void addCharacterToGame()
        {
            WorldMapCharacters wmcs = new WorldMapCharacters();
            WorldMapCharacter wmc = new TestCharacter(0);

            int id = wmcs.addCharacter(wmc);
            Assert.AreEqual(wmc, wmcs.getCharacter(id));
        }

        [TestMethod]
        public void checkCharacterIsOnTile()
        {
            WorldMap wm = new WorldMap(50, 50);
            int id = wm.addCharacterToWorldMap(0, 0);

            Assert.AreEqual(true, wm.doesTileHaveCharacter(wm.getWorldTileAt(0, 0)));
        }
    }
}
