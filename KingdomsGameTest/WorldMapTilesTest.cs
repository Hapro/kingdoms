﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KingdomsGame;
using System.Collections.Generic;

namespace KingdomsGameTest
{
    [TestClass]
    public class WorldMapTilesTest
    {
        /*
        [TestMethod]
        public void addTileToWorldMap()
        {
            WorldMapTilesTestClass wmt = new WorldMapTilesTestClass();
            WorldMapTile wt = new WorldMapTile(0);

            wmt.addWorldTileAt(wt, 0, 0);
            Assert.AreEqual(wmt.NumberOfTilesInMap, 1);
        }

        [TestMethod]
        public void doTileStoredLocationsMatch()
        {
            WorldMapTilesTestClass wmt = new WorldMapTilesTestClass();

            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 3, 5);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 4, 4);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 76, 33);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 44, 66);

            Assert.AreEqual(0, wmt.getWorldTileAt(0, 0).X);
            Assert.AreEqual(0, wmt.getWorldTileAt(0, 0).Y);

            Assert.AreEqual(3, wmt.getWorldTileAt(3, 5).X);
            Assert.AreEqual(5, wmt.getWorldTileAt(3, 5).Y);

            Assert.AreEqual(76, wmt.getWorldTileAt(76, 33).X);
            Assert.AreEqual(33, wmt.getWorldTileAt(76, 33).Y);

            Assert.AreEqual(44, wmt.getWorldTileAt(44, 66).X);
            Assert.AreEqual(66, wmt.getWorldTileAt(44, 66).Y);
        }

        [TestMethod]
        public void getValidTileFromMap()
        {
            WorldMapTilesTestClass wmt = new WorldMapTilesTestClass();

            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 3, 5);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 4, 4);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 76, 33);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 44, 66);

            Assert.AreEqual(TileType.GRASS, wmt.getWorldTileAt(0, 0).TileType);
            Assert.AreEqual(TileType.GRASS, wmt.getWorldTileAt(3, 5).TileType);
            Assert.AreEqual(TileType.GRASS, wmt.getWorldTileAt(4, 4).TileType);
            Assert.AreEqual(TileType.GRASS, wmt.getWorldTileAt(76, 33).TileType);
            Assert.AreEqual(TileType.GRASS, wmt.getWorldTileAt(44, 66).TileType);
        }

        [TestMethod]
        public void getInvalidTileFromMap()
        {
            WorldMapTilesTestClass wmt = new WorldMapTilesTestClass();
            Assert.AreEqual(null, wmt.getWorldTileAt(100, 100));
        }

        [TestMethod]
        public void testHashFunction()
        {
            //Default size 100 * 100
            WorldMapTilesTestClass wmt = new WorldMapTilesTestClass();
            Assert.AreEqual(0, wmt.hf(0, 0));
            Assert.AreEqual(232, wmt.hf(2, 32));
            Assert.AreEqual(9999, wmt.hf(99, 99));
        }

        [TestMethod]
        public void testGetSquareAdjacentTiles()
        {
            //Default size 100 * 100
            WorldMapTilesTestClass wmt = new WorldMapTilesTestClass();
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 1, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 2, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 1);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 1, 1);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 2, 1);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 2);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 1, 2);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 2, 2);

            List<WorldMapTile> wtl = wmt.getSquareAdjacentTiles(1, 1);

            Assert.AreEqual(8, wtl.Count);
        }

        [TestMethod]
        public void testGetCrossdjacentTiles()
        {
            //Default size 100 * 100
            WorldMapTilesTestClass wmt = new WorldMapTilesTestClass();
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 1, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 2, 0);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 1);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 1, 1);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 2, 1);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 0, 2);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 1, 2);
            wmt.addWorldTileAt(new WorldMapTile(TileType.GRASS), 2, 2);

            List<WorldMapTile> wtl = wmt.getCrossAdjacentTiles(1, 1);

            Assert.AreEqual(4, wtl.Count);
        }
         */
    }
}
